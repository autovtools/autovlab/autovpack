def validate_opts(opts, config, fatal=True):
    missing_required = []
    unknown = []

    for k, v in opts.items():
        if v["required"] and not k in config:
            missing_required.append(k)
        elif not v["required"] and not k in config:
            # Apply defaults
            if "default" in v:
                config[k] = v['default']
            else:
                config[k] = None

    for k, v in config.items():
        if not k in opts:
            unknown.append(k)

    msg = ''
    if missing_required:
        msg += f"Missing required options: {missing_required}\n"
    if unknown:
        msg += f"Got unknown options: {unknown}"

    if msg:
        if fatal:
            raise TypeError(msg)
        print(msg)
        return False

    return True
