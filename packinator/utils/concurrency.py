"""
Abstraction layer for concurrency operations
    in case gevent is later swapped for a different library
"""
import os
import shutil
import shlex
import logging
import time
import itertools

import gevent
from gevent import subprocess
from gevent.pool import Pool

# def get_thread(target, *args, **kwargs):
#    return threading.Thread(target=target, daemon=True, args=args, kwargs=kwargs)

def grouper(iterable, n, fillvalue=None):
    """
    Collect data into fixed-length chunks or blocks
        https://docs.python.org/3/library/itertools.html
    """
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx
    args = [iter(iterable)] * n
    return itertools.zip_longest(fillvalue=fillvalue, *args)

def spawn(target, *args, **kwargs):
    return gevent.spawn(target, *args, **kwargs)

def kill(spawned, timeout=10):
    if not isinstance(spawned, list):
        spawned = [spawned]
    finished = gevent.joinall(spawned,  timeout=timeout)
    for child in spawned:
        if not child in finished:
            gevent.kill(child)


def get_pool(num_workers=None):
    return Pool(num_workers)

def add_to_pool(pool, target, *args, **kwargs):
    return pool.spawn(target, *args, **kwargs)

def start_pool(pool, proc_list, logger):
    # Yield to the pool
    try:
        pool.join()
        return True
    except KeyboardInterrupt:
        logger.warning("Killing {} subprocesses".format(len(proc_list)))
        # Wait a second so user can Ctrl+C again to leave artifacts
        time.sleep(1)
        for proc in proc_list:
            proc.terminate()

        logger.warning("Waiting for processes to exit cleanly...")
        time.sleep(15)
        pool.kill()
        return False


def get_packer_command(packer_config, var_files=[], user_vars={}, force=False, on_error="cleanup"):
    packer = shutil.which("packer")
    if not packer:
        raise ValueError("Cannot find packer in PATH!")

    cmd = [
        packer,
        "build",
    ]
    if force:
        cmd.append("-force")
    cmd.append(f"--on-error={on_error}")

    for var_file in var_files:
        cmd.append(f"-var-file={var_file}")

    for key, val in user_vars.items():
        cmd.append("-var")
        cmd.append(f"{key}={val}")
    cmd.append(packer_config)

    return cmd


def log_pipe(pipe, logger, level):
    with pipe:
        for line in iter(pipe.readline, b""):
            logger.log(level, line.decode().rstrip('\n'))


def run_with_logger(proc_list, cmd, logger, env=None, stdout_level=logging.INFO, stderr_level=logging.ERROR):
    """
    Run the subproccess command (given by proc_list) to completion, logging output to logger
        with the specified logging levels
    """
    if env:
        new_env = os.environ.copy()
        new_env.update(env)
        env = new_env

    logger.info(
        ' '.join(
            [shlex.quote(tok) for tok in cmd]
        )
    )

    proc = subprocess.Popen(
        cmd, env=env, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    proc_list.append(proc)

    out_logger = spawn(log_pipe, proc.stdout, logger, stdout_level)
    err_logger = spawn(log_pipe, proc.stderr, logger, stderr_level)
    try:
        gevent.joinall([out_logger, err_logger])
        if proc.wait() == 0:
            return True
        else:
            return False
    except KeyboardInterrupt:
        # Signal Packer to exit, and give it some time to delete the VMs
        # (Ctrl+C twice really fast to prevent cleanup)
        logger.warning("Got Ctrl+C, killing subprocess")
        proc.terminate()
        logger.warning("Waiting for process to exit cleanly...")
        time.sleep(15)
        kill([out_logger, err_logger])
        return False
