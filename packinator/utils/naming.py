import os
import datetime
import requests

from .cmp import *

def _join_tokens(delim, tokens):
    tokens = list(filter(None, tokens))
    return delim.join(tokens)

def get_vm_name(os_family, os_flavor, os_version=None, os_variant=None, os_arch=None):
    """
    Returns the name this VM template should be given.
    """
    return _join_tokens('-', [os_flavor, os_version, os_variant, os_arch])


def get_vm_folder(base_dir, os_family, os_flavor, os_version=None, os_variant=None, os_arch=None):
    """
    Returns the path to the folder in which this VM should be placed. 
    """
    if base_dir and base_dir.endswith('/'):
        base_dir = base_dir[:-1]
    return _join_tokens('/', [base_dir, os_family, os_flavor, os_version, os_variant, os_arch])


def get_tags(config, build_timestamp=True):
    """
    Returns a dictionary of tags to apply to a VM

    config: a VM config to generate the tags from
    build_timestamp: if True, also add a build_timestamp tag with the current time
    """
    # os_guest is included, since it is the *actual* correct os_guest value
    #  (the guestOS reported to vSphere might change to force GuestOS customization to happen)
    allowed = ["os_family", "os_flavor", "os_variant", "os_version", "os_arch", "os_guest", "customization_type"]
    tags = {}
    if build_timestamp:
        now = datetime.datetime.now().replace(microsecond=0)
        tags["build_timestamp"] = now.isoformat()
    for key in allowed:
        if key in config:
            tags[key] = config[key]

    return tags


def get_ancestors_child(sub_dir, ancestor, child):
    """
    Given an absolute path to subdirectory (subdir),
        containing an ancestor direcotry named (ancestor),
        return a path to a file named (child)
        withinn the ancestor directory

    Ex. get_ancestors_child("/a/b/c/d/e/g", "c", "my_file.sh")
        returns "/a/b/c/my_file.sh"
    """
    head = sub_dir
    tail = True
    while tail and tail != ancestor:
        head, tail = os.path.split(head)

    if tail == ancestor:
        return os.path.join(head, tail, child)

def get_os_helpers(file_dir, os_family, filename="helpers.sh"):
    return get_ancestors_child(file_dir, os_family, filename)

def get_setup_script(file_dir, path_component, filename="setup.sh"):
    return get_ancestors_child(file_dir, path_component, filename)

def get_packer_config(file_dir, path_component, filename="packer.json"):
    return get_ancestors_child(file_dir, path_component, filename)

def get_preseed_url(file_url, path_component, filename="preseed.cfg"):
    return get_ancestors_child(file_url, path_component, filename)

def get_centos_repo(os_version, os_arch):
    arch = "x86_64"
    if os_arch == "x86":
        arch = "i386"
    version_tuple = os_version.split(".")
    cmp_ver = get_comparable_version(os_version)
    major = cmp_ver[0]
    if cmp_ver >= get_comparable_version("8"):
        # The layout is differnt
        #   Assume CentOS 9+ will use this format
        suffix = f"{os_version}/BaseOS/{arch}/os/"
        # Only the most recent 2 releases are kept on mirror,
        #   then they are moved to vault
        # Prefer vault (as it should never change) and then check mirror
        #   if it wasn't found on vault
        vault_url = f"http://vault.centos.org/{suffix}"
        mirror_url = f"http://mirror.centos.org/centos-{major}/{suffix}"
        resp = requests.get(vault_url)
        if resp.ok:
            return vault_url
        else:
            resp = requests.get(mirror_url)
            if resp.ok:
                return mirror_url
            else:
                raise Exception(f"Could not reach {vault_url} or {mirror_url}!")
    else:
        # Uses old repo layout; everything is on vault
        return f"http://vault.centos.org/{os_version}/os/{arch}/"

def get_epel_repo(os_version, os_arch):
    arch = "x86_64"
    if os_arch == "x86":
        arch = "i386"
    cmp_ver = get_comparable_version(os_version)
    major = cmp_ver[0]
    if cmp_ver >= get_comparable_version("8"):
        # The layout is differnt
        #   Asusme CentOS 9+ will use this format
        return f"http://dl.fedoraproject.org/pub/epel/{major}/Everything/{arch}/"
    else:
        return f"http://dl.fedoraproject.org/pub/epel/{major}/{arch}/"

def _get_fedora_repo(os_version, os_arch, updates=False):
    arch = "x86_64"
    if os_arch == "x86":
        arch = "i386"
    cmp_ver = get_comparable_version(os_version)
    major = cmp_ver[0]
    if version_lt(os_version, "30"):
        # Archived
        if updates:
            return f"http://dl.fedoraproject.org/pub/archive/fedora/linux/updates/{major}/{arch}/"
        else:
            return f"http://dl.fedoraproject.org/pub/archive/fedora/linux/releases/{major}/Everything/{arch}/os/"
    else:
        # Current
        if updates:
            return f"http://dl.fedoraproject.org/pub/fedora/linux/updates/{major}/{arch}/"
        else:
            return f"http://dl.fedoraproject.org/pub/fedora/linux/releases/{major}/Everything/{arch}/os/"

def get_fedora_repo(os_version, os_arch):
    return _get_fedora_repo(os_version, os_arch)

def get_fedora_updates_repo(os_version, os_arch):
    return _get_fedora_repo(os_version, os_arch, updates=True)

def get_opensuse_repo(os_version, os_arch):
    # 42 < 15: https://en.wikipedia.org/wiki/OpenSUSE_version_history
    cmp_ver = get_comparable_version(os_version)
    major = cmp_ver[0]
    if version_lt(os_version, "15") and major != 42:
        # Use the (official?) archive
        return f"http://ftp5.gwdg.de/pub/opensuse/discontinued/distribution/{os_version}/repo/oss/"
    else:
        # 15 is current, not on archive
        return f"http://download.opensuse.org/distribution/leap/{os_version}/repo/oss/"

def get_freebsd_packagesite(os_version, arch):
    cmp_ver = get_comparable_version(os_version)
    major = cmp_ver[0]
    return f"http://pkg.freebsd.org/FreeBSD:{major}:{arch}/release_0/"

def get_freebsd_distsite(os_version, arch):
    # The oldest version on https://download.freebsd.org/ftp/releases/amd64/
    #  (older are in the archive)
    CURRENT="11.2"
    if version_lt(os_version, CURRENT):
        return f"ftp://ftp-archive.freebsd.org/pub/FreeBSD-Archive/old-releases/{arch}/{os_version}-RELEASE"
    else:
        return f"ftp://ftp.freebsd.org/pub/FreeBSD/releases/{arch}/{os_version}-RELEASE"
        
def get_freebsd_environment(os_version, os_arch):
    # Get it from the archive
    arch = "amd64"
    if os_arch == "x86":
        arch = "i386"
    cmd = ";".join( [
            'export PACKAGESITE="{}"'.format(get_freebsd_packagesite(os_version, arch)),
            'export BSDINSTALL_DISTSITE="{}"'.format(get_freebsd_distsite(os_version, arch))
        ]
    )
    return cmd

def get_freebsd_files(base_url):
    return [
        os.path.join(base_url, name) for name in ["installer.cfg", "install.sh"]
    ]
