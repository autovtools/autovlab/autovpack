import os
import yaml
import shlex
import hashlib
import itertools

import jinja2
from termcolor import cprint


def sort_dict(obj):
    """
    Sorts a dictionary (obj) by its keys and returns a new dict.
    """
    _obj = {}
    for key in sorted(obj.keys()):
        _obj[key] = obj[key]
    return _obj

def _load_config(config, is_file=True):
    content = config
    if is_file:
        with open(config, "r") as f:
            content = f.read()

    return yaml.safe_load(content)

def try_load(config, strict=True, is_file=True):
    try:
        return _load_config(config, is_file=is_file)
    except ValueError as e:
        # strict=False -> ignore invalid JSON files
        if strict:
            raise(e)

def load_config(config, strict=True, disable_jinja=False, jinja_funcs={}, overrides=None):
    res = {}
    if isinstance(config, dict):
        return config

    # It's a filename
    is_file = True
    config = os.path.abspath(os.path.expanduser(config))
    if not disable_jinja:
        search_dir, basename = os.path.split(config)
        basename = os.path.basename(config)
        jinja_env = jinja2.Environment(
            loader=jinja2.FileSystemLoader(search_dir),
            cache_size=0
        )

        for func_name, func in jinja_funcs.items():
            jinja_env.globals[func_name] = func

        template = jinja_env.get_template(os.path.basename(basename))
        config = template.render()
        is_file = False

    res = try_load(config, strict=strict, is_file=is_file)
    if overrides:
        for key, val in overrides.items():
            if key in res:
                res[key] = val
    return res

def explode_config(config):
    """
    Given a config dictionary containg values that are strings or lists of strings,
        return a list of dictionaries with values that are strings and every combination
        of formerly list values is represented.

    Ex. The following config: {
        ...
        os_flavor="Ubuntu"
        os_version="X.Y.Z"
        os_arch = ["x64", "x86"],
        os_variant = ["Desktop", "Server"]
        ...   
    }
    Represents 4 configs, Ubuntu-X.Y.Z-Server-x64, Ubuntu-X.Y.Z-Server-x86, Ubuntu-X.Y.Z-Desktop-x64, Ubuntu-X.Y.Z-Desktop-x86

    Use carefully, combinations explode fast and you may end up with massive builds.
    Just because you *can* build every possible OS doesn't mean you should.

    NOTE: Currently, it only makes sense to provide more than one os_variant in a config file,
        since you need a different ISO for each architecture.
    """

    config_list = []
    list_keys = {
        key: value for key, value in config.items() if isinstance(value, list)
    }
    if not list_keys:
        return [config]

    # Produce something like:  [('Server', 'x86'), ('Server', 'x64'), ('Desktop', 'x86'), ('Desktop', 'x64')]
    for combination in itertools.product(*list_keys.values()):
        conf = config.copy()
        # Each combination gives the next (string) values for every key in  list_keys
        for key, value in zip(list_keys.keys(), combination):
            conf[key] = value
        config_list.append(conf)

    return config_list


def pprint_config(config, indent="", spaces=4, more=False, sort_keys=False):
    """
    Helper to pretty print config information
    """
    censored = ["_user", "_password", "_hash"]
    if not config:
        cprint("{ }", "magenta")
        return
    if sort_keys:
        config = sort_dict(config)

    max_len = max([len(key) for key in config.keys()])
    # 2 quotes + 1 for padding
    max_len += 3
    inner_indent = indent + " "*spaces

    cprint(indent + "{", "magenta")
    curr = 0
    num_keys = len(config)
    for key, value in config.items():
        if any(key.endswith(ending) for ending in censored):
            value = "..."
        key = f'"{key}"'.ljust(max_len)
        cprint(f"{inner_indent}{key}", "yellow", end=": ")
        value_color = "red"
        _more = curr < (num_keys - 1)
        if isinstance(value, dict):
            pprint_config(value, indent=indent+" "*spaces, more=_more)
        else:
            if isinstance(value, str):
                quoted = shlex.quote(value)
                if value == quoted:
                    # No quotes were added
                    value = f'"{value}"'
            else:
                value_color = "cyan"
                value = str(value)

            end = ",\n"
            if not _more:
                end = "\n"

            cprint(indent + value, value_color, end=end)
        curr += 1
    end = "}"
    if more:
        # Caller has more
        end = "},"
    cprint(indent + end, "magenta")


def compute_hash(filename):
    """
    Compute multiple standard hashes at once, and return a list of them
    """
    chunk_size = 65536
    hashers = [hashlib.md5(), hashlib.sha1(), hashlib.sha256()]
    with open(filename, "rb") as in_file:
        while True:
            data = in_file.read(chunk_size)
            if not data:
                break
            for hasher in hashers:
                hasher.update(data)
    return [hasher.hexdigest() for hasher in hashers]

def verify_file(filename, expected_hash):
    """
    Returns a tuple (is_valid, [actual_hash1, actual_hash2, ...] )
    """
    hashes = compute_hash(filename)
    return (expected_hash in hashes, hashes)

def waitify(token, delim=" "):
    if delim is None:
        return "<wait>".join(
            list(token)
        )
    else:
        tokens = token.split(delim)
        res = token
        if len(tokens) > 1:
            res = f"<wait>{delim}".join(
                tokens
            )
        else:
            res += "<wait>"
        return res
