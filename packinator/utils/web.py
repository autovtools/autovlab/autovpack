"""Basic utils to do web stuff

"""

import os
import shutil
import socket
import tempfile
import subprocess
import gzip

import requests

from packinator.utils import *

def get_default_ip():
    """Get our default IP by sending a garbage packet and seeing where it went

    """
    # https://stackoverflow.com/questions/166506/finding-local-ip-addresses-using-pythons-stdlib
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.connect(("169.254.255.255", 0))
    return sock.getsockname()[0]

def _copy_file(src_path=None, src_file=None, dest_path=None, dest_file=None):
    """Copy src to dest, supporting string paths or open file objects

    """
    if src_path:
        src_file = open(src_path, "rb")

    if dest_path:
        with open(dest_path, "wb") as dest_file:
            shutil.copyfileobj(src_file, dest_file)
    else:
        shutil.copyfileobj(src_file, dest_file)

    if src_path and src_file:
        src_file.close()

def _download_file(file_url, out_path=None, out_file=None, allow_redirects=True):
    headers = {
        "User-Agent":"Wget/1.15 (linux-gnu)"
    }
    r = requests.get(file_url, stream=True, allow_redirects=allow_redirects, headers=headers)
    _copy_file(src_file=r.raw, dest_path=out_path, dest_file=out_file)

def download_file(file_url, out_path=None, out_file=None):
    if out_path and out_file:
        raise ValueError(
            "Cannot use both out_path and out_file simultaneously")
    if not (out_path or out_file):
        raise ValueError("Must set out_path or out_file")

    if file_url.endswith(".jigdo"):
        return download_jigdo(file_url, out_path, out_file) 
    elif file_url.endswith(".gz"):
        return download_gz(file_url, out_path, out_file)
    else:
        return _download_file(file_url, out_path, out_file)

def download_gz(gz_url, out_path=None, out_file=None):
    with tempfile.NamedTemporaryFile() as tmp_file:
        _download_file(gz_url, out_file=tmp_file)
        tmp_file.flush()
        with gzip.open(tmp_file.name, "rb") as gz_file:
            _copy_file(src_file=gz_file, dest_path=out_path, dest_file=out_file)

def download_jigdo(jigdo_url, out_path=None, out_file=None):
    """Download jigdo files, for when ISO downloads are not available

    Debian archive doesn't provide direct ISO downloads for old versions
        See https://cdimage.debian.org/cdimage/archive/

    Use jigdo-lite (from jigdo-files package in Ubuntu) to reconstruct the iso from jigdo
    Put the final iso where our caller asked us to.
    """
    if not shutil.which("jigdo-lite"):
        raise Exception("External dependency 'jigdo-lite' is missing or not in PATH!")

    # Use a known-good mirror
    #  Default is use the mirror in /etc/apt/sources.list, which might be Ubuntu
    debian_mirror = "http://ftp.us.debian.org/debian/"

    # Leave "files to scan" blank, enter debian_mirror as mirror
    _input = f"\n{debian_mirror}".encode()
    proc_list = [
        "jigdo-lite",
        jigdo_url
    ]
    # Jigdo doesn't clean up after itself, so run in a tempdir
    with tempfile.TemporaryDirectory() as tmp_dir:
        # Compute where jigdo-lite is going to put our iso
        iso_name = os.path.basename(jigdo_url)
        iso_name = os.path.splitext(iso_name)[0] + ".iso"
        iso_path = os.path.join(tmp_dir, iso_name)
        subprocess.run(proc_list, input=_input, cwd=tmp_dir, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        _copy_file(src_path=iso_path, dest_path=out_path, dest_file=out_file)

