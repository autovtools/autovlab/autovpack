import os
import requests
import lxml.html

def _get_links(base_url):
    req = requests.get(base_url)
    dom = lxml.html.fromstring(req.content.decode())                                       
    return dom.xpath('//a/@href')

# These functions can be called from jinja templates (packinator .yml files)
def get_latest_fcos():
    """Since archive mirrors for CoreOS are not maintained or desireable, always download latest
    
    Returns iso_url, iso_checksum, os_version
    """
    # Ironically, the 'stable' stream is more likely to fail to install, because:
    #   https://github.com/coreos/rpm-ostree/issues/415
    # TL;DR installing packages doesn't work if dependencies are too new for your stream
    stream = "testing"
    stream_url = f"https://builds.coreos.fedoraproject.org/streams/{stream}.json"
    req = requests.get(stream_url)
    if not req.ok:
        raise Exception(f"Failed to get lastest FCOS ISO from {stream_url}: HTTP {req.status_code}")
    stable = req.json()
    # Bare Metal gives us the ISO
    metal = stable["architectures"]["x86_64"]["artifacts"]["metal"]
    os_version = metal["release"]

    iso = metal["formats"]["iso"]["disk"]
    iso_url = iso["location"]
    iso_checksum = iso["sha256"]

    return iso_url, iso_checksum, os_version

def _get_kali(base_url, arch):
    checksum_url = os.path.join(base_url, "SHA256SUMS")
    links = _get_links(base_url)
    iso_name = None
    for name in links:
        if f"netinst-{arch}" in name:
            iso_name = name
            break
    req = requests.get(checksum_url)
    iso_checksum = None
    # These checksums can be outdated, so just default to no validation
    #   if we can't find the right checksum
    for line in req.content.decode().splitlines():
        if iso_name in line:
            tokens = [tok for tok in line.split(" ") if tok]
            iso_checksum = tokens[0]
            break
    iso_url = os.path.join(base_url, iso_name)
    # Slice the version out:
    # kali-linux-XXXX-WYY-installer-netinst-amd64.iso
    os_version = iso_name.split("kali-linux-")[1].split("-installer")[0]
    return iso_url, iso_checksum, os_version
    
def get_rolling_kali(arch="amd64"):
    """
    Use weekly iso for rolling Kali builds.

    Returns iso_url, iso_checksum, os_version
    """
    return _get_kali("https://cdimage.kali.org/kali-weekly/", arch)

def get_current_kali(arch="amd64"):
    """
    Get lastest stable / official release.

    Returns iso_url, iso_checksum, os_version
    """
    return _get_kali("https://cdimage.kali.org/current/", arch)
