#!/usr/bin/env python3

import os
import logging

import flask
from jinja2 import TemplateNotFound
from gevent.pywsgi import WSGIServer


def get_file_server_app(serve_dir, user_vars={}, render_extensions=[], logger=None):

    app = flask.Flask(__name__, template_folder=serve_dir)
    # Stop flask from caching files (e.g. scripts / preseed files we might be editing)
    app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    if logger is not None:
        logging.getLogger('werkzeug').disabled = True
        app.logger.disabled = True
        app.logger = logger

    # https://security.openstack.org/guidelines/dg_using-file-paths.html
    def is_safe_path(basedir, path, follow_symlinks=True):
        # resolves symbolic links
        if follow_symlinks:
            return os.path.realpath(path).startswith(basedir)

        return os.path.abspath(path).startswith(basedir)

    @app.route('/<path:path>')
    def serve_file(path=None):
        # Don't serve files with these extensions
        hidden_extensions = [".json"]

        filename, ext = os.path.splitext(flask.request.path)

        # Try to stop directory traversal
        #  and possible brute-force discovery of full path to serve_dir

        app.logger.info(f"GET {path}")
        # Die if the requested dir is not relative to serve_dir
        joined_path = flask.safe_join(serve_dir, path)

        # Be paranoid; check again
        if is_safe_path(serve_dir, joined_path):
            app.logger.info(f"Allowing {joined_path}")
        else:
            app.logger.info(f"Rejecting {joined_path}")
            flask.abort(404)

        if ext in hidden_extensions:
            app.logger.info(f"Extension {ext} is hidden")
            flask.abort(404)

        if ext in render_extensions:
            app.logger.info(f"Rendering {path}")
            try:
                # render_template is implicitly relative to the templates dir
                return flask.render_template(flask.request.path, **user_vars)
            except TemplateNotFound:
                flask.abort(404)
        else:
            app.logger.debug(f"{ext} is not in {render_extensions}")
            if os.path.exists(joined_path):
                app.logger.info(f"Serving {path}")
                return flask.send_file(joined_path)
            else:
                app.logger.info(f"File not found")
                flask.abort(404)

    return app


def get_file_server(serve_dir, serve_ip, serve_port, user_vars={}, logger=None, render_extensions=None):
    if render_extensions is None:
        render_extensions = get_render_extensions()

    app = get_file_server_app(
        serve_dir,
        user_vars=user_vars,
        logger=logger,
        render_extensions=render_extensions
    )
    server = WSGIServer((serve_ip, serve_port), app, log=logger)
    return server


def get_render_extensions():
    """
    Return the list of all file extensions that should be potentially rendered by flask.

    """
    return [
        ".sh",
        ".cfg",
        ".list",
        ".repo",
        ".xml",
        ".ign"
    ]
