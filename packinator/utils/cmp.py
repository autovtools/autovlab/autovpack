import re
def get_comparable_version(version_str):
    """
    Given a version string like Major.Minor.Revision,
    return an object that can be compared semtantically to other such objects

    Ex. get_comparable_version("6.2.49") > get_comparable_version("6.0")
    """
    # Replace non-numeric sequences with dots to allow semantic comparision
    #   e.g. 2.0-rc1 -> 2.0.1
    version_str = re.sub("[^0-9.]+", ".", version_str)
    # Drops any non-numeric 
    return tuple(int(tok) for tok in version_str.split(".") if tok)

# Version comparison
#   (needed for some of the naming)

def _version_cmp(ver1, ver2):
    ver1 = get_comparable_version(ver1)
    ver2 = get_comparable_version(ver2)
    min_len = min(len(ver1), len(ver2))
    return ver1[:min_len], ver2[:min_len]

def version_eq(ver1, ver2):
    ver1, ver2 = _version_cmp(ver1, ver2)
    return ver1 == ver2

def version_lt(ver1, ver2):
    ver1, ver2 = _version_cmp(ver1, ver2)
    return ver1 < ver2

def version_le(ver1, ver2):
    ver1, ver2 = _version_cmp(ver1, ver2)
    return ver1 <= ver2

def version_gt(ver1, ver2):
    ver1, ver2 = _version_cmp(ver1, ver2)
    return ver1 > ver2

def version_ge(ver1, ver2):
    ver1, ver2 = _version_cmp(ver1, ver2)
    return ver1 >= ver2

def version_in_range(ver, lower, upper, lower_inclusive=True, upper_inclusive=True):
    left_res = False
    right_res = False
    if lower_inclusive:
        left_res = version_ge(ver, lower)
    else:
        left_res = version_gt(ver, lower)
    if not left_res:
        return False

    if upper_inclusive:
        right_res = version_le(ver, upper)
    else:
        right_res = version_lt(ver, upper)

    return left_res and right_res
