"""
Basic utils to simplify + standarize logging
"""
import logging

import coloredlogs


def _add_handler(logger, handler, fmt, level):
    handler.setLevel(level)
    handler.setFormatter(fmt)
    logger.addHandler(handler)

# name=self.__class__.__name__


def get_logger(name=None, level=logging.INFO, log_file=None):
    logger = logging.getLogger(name)
    if len(logger.handlers) == 0:
        logger.setLevel(level)
        logger.propagate = False

        extra = ""
        # Add line info for debug logging
        if level <= logging.DEBUG:
            extra = " - %(filename)s:%(funcName)s:%(lineno)d"

        fmt_str = (
            "%(asctime)s"
            " - %(levelname)s"
            " - %(name)s" + extra +
            " - %(message)s"
        )

        # Set up Console Handler (prints events to STDERR)
        coloredlogs.install(fmt=fmt_str, level=level, logger=logger)

        if log_file:
            fmt = logging.Formatter(fmt_str)
            # Set up File Handler (prints events to STDOUT)
            _add_handler(logger, logging.FileHandler(log_file), fmt, level)
    return logger
