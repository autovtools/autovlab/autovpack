#!/usr/bin/env python3
PACKINATOR_OPTS = {
    "vcenter_server": {
        "required": True,
        "description": "IP / DNS name of vCenter instance"
    },
    "vsphere_cluster": {
        "required": False,
        "description": "Name of the vSphere cluster to use"
    },
    "esxi_host": {
        "required": False,
        "description": "IP / DNS name of esxi host"
    },
    "vsphere_dc": {
        "required": True,
        "description": "Name of vSphere Datacenter"
    },
    "insecure_connection": {
        "required": False,
        "description": "Allow insecure connections (e.g. self-signed certs), default: 'True'",
        "default": "True"
    },
    "templates_root": {
        "required": True,
        "description": "The root VM folder that artifacts should be placed under. (You must make the subfolders)"
    },
    "vsphere_datastore": {
        "required": True,
        "description": "The vSphere datastore that should be used for artifacts"
    },
    "iso_root": {
        "required": False,
        "description": "The datastore ISO folder. Checked before re-downloading ISOs; downloaded ISOs are uploaded here."
    },
    "serve_ip": {
        "required": False,
        "description": "The ip to serve files on (default: 0.0.0.0)",
        "default": "0.0.0.0"
    },
    "serve_port": {
        "required": False,
        "description": "The port to serve files on. (default: 8080)",
        "default": "8080"
    },
    "serve_dir": {
        "required": False,
        "description": "The file directory to serve"
    },
    "network": {
        "required": True,
        "description": "The vSphere port group to connect artifacts to whlie they are being provisioned. (Needs outbound internet)"
    },
    "desktop_vram": {
        "required": False,
        "description": "Amount of video ram for non-headless VMs. (>=32 for 4k support)",
        "default": "32"
    },
    "server_vram": {
        "required": False,
        "description": "Amount of video ram for headless VMs",
        "default": "4"
    },
    "linux_ram": {
        "required": False,
        "description": "The amount of RAM (MB) to give *nix VMs"
    },
    "linux_cpus": {
        "required": False,
        "description": "The number of vCPUs to give *nix VMs"
    },
    "linux_disk": {
        "required": False,
        "description": "The disk size (MB) for *nix VMs"
    },
    "windows_cpus": {
        "required": False,
        "description": "The number of vCPUs to give Windows VMs"
    },
    "windows_ram": {
        "required": False,
        "description": "The amount of RAM (MB) to give Windows VMs"
    },
    "windows_disk": {
        "required": False,
        "description": "The disk size (MB) for Windows VMs"
    },
    "log_level": {
        "required": False,
        "description": "The string log level used by logging module to control log verbosity. [DEBUG, INFO, WARNING, ERROR, CRITICAL]"
    },
    "force": {
        "required": False,
        "description": "Delete VMs if they already exist",
        "default": False
    },
    "ip_wait_timeout": {
        "required": False,
        "description": "Packer option; How long to wait for a VM's IP to become available (i.e. exposed to VMware tools). Default: 1h",
        "default": "1h"
    },
    "ssh_wait_timeout": {
        "required": False,
        "description": "Packer option; How long to wait for a VM to accept ssh connections after its IP became available. Default: 1h",
        "default": "1h"
    },
}
