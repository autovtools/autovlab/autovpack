VM_OPTS = {
    'py_class': {
        "required": True,
        "description": "The python class to instantiate to manage the creation of this VM."
    },
    'iso_url': {
        "required": True,
        "description": "URL to installation iso"
    },
    'iso_name': {
        "required": False,
        "description": "Actual filename of iso. Use if iso_url doesn't end in the filename"
    },
    'iso_checksum': {
        "required": False,
        "description": "Checksum of the installation iso"
    },
    'iso_path': {
        "required": False,
        "description": "Full vSphere Datastore path for the installation iso."
    },
    'os_family': {
        "required": True,
        "description": "e.g. Linux, Windows, BSD, Other"
    },
    'os_flavor': {
        "required": True,
        "description": "Linux distro / Windows version"
    },
    'os_variant': {
        "required": False,
        "description": "Variant of the os (e.g Server / Desktop)"
    },
    'os_version': {
        "required": True,
        "description": "Linux release / Windows build number"
    },
    'os_arch': {
        "required": True,
        "description": "CPU architecture of the image (e.g x86 / x86_64)"
    },
    'os_guest': {
        "required": True,
        "description": "The VMware GuestOSType for GuestOS Customization."
    },
    'customization_type': {
        "required": False,
        "description": "The type of Guest OS customization to perform. [linux, windows, vapp, none] default: defined per VM subclass."
    },
    'helpers_dest': {
        "required": False,
        "description": "Remote path to place helper functions needed by setup scripts",
        "default": "/tmp/.packer_helpers.sh"
    },
    'log_level': {
        "required": False,
        "description": "The string log level used by logging module to control. [DEBUG, INFO, WARNING, ERROR, CRITICAL]"
    },
    'helpers_src': {
        "required": False,
        "description": "Override the path to helper functions available to setup_script.",
    },
    'setup_script': {
        "required": False,
        "description": "Override the path to the setup / install script for this VM.",
    },
    'packer_config': {
        "required": False,
        "description": "Override the path to the packer template to use.",
    },
    'unattended_cfg': {
        "required": False,
        "description": "Override the path to the preeseed.cfg / kickstart.cfg (or equivalent) file to use.",
    }
}
