#!/usr/bin/env python3
import os
import logging

import bcrypt

from terrashell import TerraShell

import packinator.vm
from packinator.vm import VM
from packinator.opts import PACKINATOR_OPTS
import packinator.utils as utils
from packinator.utils import jinja_funcs


class Packinator():
    """
    Wrapper for Packer capable of building multiple VMs
    """
    opts = PACKINATOR_OPTS

    def __init__(self, config, serve_vars=[], packer_vars=[],
                 force=False, on_error="cleanup", log_level=logging.INFO,
                 log_file=None, packer_config=None,
                 variants=None, architectures=None,
                 batch_size=None, disable_jinja=False,
                 overrides=None):
        self.log_file = log_file
        self.log_level = log_level

        self.packer_config = packer_config
        self.force = force
        self.on_error = on_error
        self.disable_jinja = disable_jinja

        self.pool = None
        self.proc_list = []
        self.workers = {}

        self.vms = []
        self.serve_vars = serve_vars
        self.packer_vars = packer_vars

        self.variants = variants
        self.architectures = architectures
        self.batch_size = batch_size

        self.jinja_funcs = {"jinja_funcs":jinja_funcs}
        self.overrides = overrides

        self.logger = utils.get_logger(
            name=self.__class__.__name__,
            level=log_level,
            log_file=self.log_file
        )

        if isinstance(config, str):
            self.logger.debug(f"Loading config from {config}...")
            config = utils.load_config(
                config,
                disable_jinja=self.disable_jinja,
                jinja_funcs=self.jinja_funcs,
                overrides=self.overrides
            )

        self.config = config

        if config.get("log_level", None):
            self.log_level = logging.getLevelName(config["log_level"])
            self.logger.setLevel(self.log_level)

        self.config["log_level"] = self.log_level

        utils.validate_opts(self.opts, self.config)
        self.compute_fields()

        self.terrashell = TerraShell(
            log_level   =   self.log_level,
            log_file    =   self.log_file
        )
        if self.logger.getEffectiveLevel() <= logging.DEBUG:
            utils.pprint_config(self.config)
        self.logger.debug("Initialization complete")

    def compute_fields(self):
        serve_dir = self.config.get("serve_dir", None)
        if serve_dir:
            if not os.path.isabs(serve_dir):
                serve_dir = os.path.expanduser(serve_dir)
            if not os.path.isabs(serve_dir):
                serve_dir = os.path.abspath(
                    os.path.join(
                        os.getcwd(),
                        serve_dir
                    )
                )
        self.config["serve_dir"] = serve_dir

        # Type marshalling (packer requires everyting be string in the config file)
        self.config["serve_port"] = int(self.config["serve_port"])

    def get_log_level(self):
        return self.config["log_level"]

    def iso_exists(self, file_path, os_family, os_flavor):
        """
        Returns (True, computed_path) if it exists,
        or (False, computed_path) if it does not 
        """
        iso_path = self.get_ds_iso_path(
            os.path.basename(file_path),
            os_family,
            os_flavor
        )
        return (self.terrashell.test_path(iso_path), iso_path)

    def upload_iso(self, file_path, os_family, os_flavor, upload_name=None, force=False):
        """
        Uploads the iso at file_path (local) to the datastore
        """
        if upload_name is None:
            upload_name = file_path
        exists, iso_path = self.iso_exists(upload_name, os_family, os_flavor)

        if exists and not force:
            return True

        return self.terrashell.copy_datastoreitem(file_path, iso_path)

    def get_vm_name(self, os_family, os_flavor, os_version=None, os_variant=None, os_arch=None):
        return utils.get_vm_name(os_family, os_flavor, os_variant, os_version, os_arch)

    def get_vm_folder(self, os_family, os_flavor, os_version=None, os_variant=None, os_arch=None):
        return utils.get_vm_folder(self.config["templates_root"], os_family, os_flavor)

    def get_server_url(self, path=""):
        """
        Retuns a url to the file server.
        """
        ip = self.config["serve_ip"]
        if ip == "0.0.0.0":
            ip = utils.get_default_ip()

        port = self.config['serve_port']
        return f"http://{ip}:{port}/{path}"

    def get_file_url(self, os_family, os_flavor, os_version=None, os_variant=None, os_arch=None):
        """
        Returns the file_server url for the specified VM's files.

        NOTE: the caller must know if this VM breaks down its config files
            by variant, version, and arch (do not pass these arguments if it does not)
        """
        path = utils.get_vm_folder(
            None,
            os_family,
            os_flavor,
            os_version=os_version,
            os_variant=os_variant,
            os_arch=os_arch
        )
        return self.get_server_url(path=path)

    def get_file_dir(self, os_family, os_flavor, os_version=None, os_variant=None, os_arch=None):
        """
        Returns the local file path for the specified VM's files.

        NOTE: the caller must know if this VM breaks down its config files
            by variant, version, and arch (do not pass these arguments if it does not)
        """
        return utils.get_vm_folder(
            self.config['serve_dir'],
            os_family,
            os_flavor,
            os_version=os_version,
            os_variant=os_variant,
            os_arch=os_arch
        )

    def vm_exists(self, vm_folder, vm_name):
        return self.terrashell.vm_exists(vm_name=vm_name, root_folder=vm_folder)

    def prep_template(self, vm_folder, vm_name, vm_config):
        """
        Preps the template and applies default tags
        """
        tags = utils.get_tags(vm_config)
        res2 = self.apply_tags(vm_folder, vm_name, tags)

        res = self.terrashell.prep_template(vm_folder, vm_name)
        return res and res2

    def enable_vmwarehardware(self, vm_folder, vm_name):
        """
        Sets hardware to VMware-optimized version.
        """
        return self.terrashell.enable_vmwarehardware(vm_folder, vm_name)

    def enable_ovf_env(self, vm_folder, vm_name):
        """
        Enables the Ovf Environment, exposing extra info to the GuestOS
        """
        return self.terrashell.enable_ovf_env(vm_folder, vm_name)

    def remove_vm(self, vm_folder, vm_name):
        return self.terrashell.remove_vm(vm_folder, vm_name)

    def apply_tags(self, vm_folder, vm_name, tags):
        return self.terrashell.apply_tags(vm_folder, vm_name, tags)

    def get_ds_iso_path(self, file_name, os_family, os_flavor):
        """
        Returns an ISO path suitable for use by terrashell
        """
        iso_prefix, iso_ext = os.path.splitext(file_name)
        if iso_ext != ".iso":
            # Handle special cases like .jigdo
            file_name = iso_prefix + ".iso"
        return "vmstore:/{}/{}/{}/{}/{}/{}".format(
            self.config["vsphere_dc"],
            self.config["vsphere_datastore"],
            self.config["iso_root"],
            os_family,
            os_flavor,
            file_name
        )

    def get_iso_path(self, os_family, os_flavor, iso_name=None):
        """
        Returns an ISO path suitable for use by packer
        """
        path = utils.get_vm_folder(
            self.config["iso_root"], os_family, os_flavor)
        if iso_name:
            iso_prefix, iso_ext = os.path.splitext(iso_name)
            if iso_ext != ".iso":
                # Handle special cases like .jigdo
                iso_name = iso_prefix + ".iso"
            path = os.path.join(path, iso_name)
        return "[{}] {}".format(self.config["vsphere_datastore"], path)

    def _build(self, vm_batch, ensure_iso=True, build=True, post_build=True):
        # Since we might be building many VMs based on the same isos,
        #   Call ensure_iso once per unique iso_path
        unique_isos = {}
        for vm_obj in vm_batch:
            unique_isos[
                vm_obj.config["iso_path"]
            ] = vm_obj


        if ensure_iso:
            # Ensure we have all the isos we need for this build first
            for vm_obj in unique_isos.values():
                utils.add_to_pool(self.pool, vm_obj.ensure_iso)
            if not utils.start_pool(self.pool, self.proc_list, self.logger):
                return False

        if build:
            # Do a normal build
            # (if user wanted to ensure_iso's, we already did above)
            for vm_obj in vm_batch:
                utils.add_to_pool(self.pool, vm_obj.build,
                                  force=self.force, ensure_iso=False)
        elif post_build:
            # Instead of building, only do the post-build step 
            for vm_obj in vm_batch:
                utils.add_to_pool(self.pool, vm_obj.post_build)

        else:
            # User didn't want to build or post_build
            #   The pool is empty, so report success
            return True

        # We have tasks in the pool; actually do them
        if not utils.start_pool(self.pool, self.proc_list, self.logger):
            return False

        return True

    def build(self, *args, ensure_iso=True, build=True, post_build=True):
        print(f"Building {args}")

        for conf_file in args:
            config = utils.load_config(
                conf_file,
                disable_jinja=self.disable_jinja,
                jinja_funcs=self.jinja_funcs,
                overrides=self.overrides
            )
            if self.logger.getEffectiveLevel() <= logging.DEBUG:
                utils.pprint_config(config)

            if "py_class" not in config:
                msg = f"{conf_file} does not contain 'py_class'; Is it a valid Packinator VM configuration?"
                self.logger.error(msg)
                if self.logger.getEffectiveLevel() <= logging.ERROR:
                    utils.pprint_config(config)
                raise TypeError(msg)

            # Safer than a true dynamic import, but probably still expliotable
            vm_class = getattr(packinator.vm, config["py_class"])
            # Try to stop shenanigans with __builtins__, etc
            if not issubclass(vm_class, VM):
                msg = f"{conf_file}:py_class ({config['py_class']}) is not a Packinator VM!"
                self.logger.error(msg)
                raise TypeError(msg)

            config_list = utils.explode_config(config)
            self.logger.debug(
                f"config_file {conf_file} exploded into {len(config_list)} configs")
            if self.variants:
                # Filter out any variants the user does not want to build right now
                config_list = [
                    config for config in config_list if config["os_variant"] in self.variants]

            if self.architectures:
                # Filter out any architectures the user does not want to build right now
                config_list = [
                    config for config in config_list if config["os_arch"] in self.architectures]
            
            self.logger.debug(
                f"{len(config_list)} variants were requested")

            for config in config_list:
                self.vms.append(
                    vm_class(
                        config,
                        self,
                        log_level=self.config["log_level"],
                        log_file=self.log_file
                    )
                )
        num_vms = len(self.vms)
        self.logger.info(f"Loaded {num_vms} vms!")

        # Create the processing pool
        self.pool = utils.get_pool()
        if build:
            # We are actually building, so start the server
            try:
                self.start_server()
            except OSError as e:
                self.logger.error(f"Failed to start file server: {e}")
            
        if build and self.batch_size and not self.force:
            # Optimization: Any VMs that already exist will limit our parallelism
            # (they will take a spot in the batch, but immediately finish)
            self.logger.debug("Filtering out VMs that already exist")
            self.vms = [
                vm for vm in self.vms
                if not self.vm_exists(
                    vm.config["vm_folder"],
                    vm.config["vm_name"]
                )
            ]
            self.logger.info(f"{len(self.vms)} VM(s) do not exist and will be built")

        if self.batch_size:
            # Throttle the build to at most batch_size
            # (useful when IOPS are limited or downloads are throttled / blacklisted)
            for vm_batch in utils.grouper(self.vms, self.batch_size):
                # Filter out Nones on the last group
                vm_batch = [vm for vm in vm_batch if not vm is None]
                self.logger.info(f"Processing batch: {len(vm_batch)} vms...")
                if not self._build(vm_batch, ensure_iso=ensure_iso, build=build, post_build=post_build):
                    # User cancelled
                    return False
        else:
            return self._build(self.vms, ensure_iso=ensure_iso, build=build, post_build=post_build)

        return True

    def insert_hash(self, config_vars):
        vm_password = config_vars.get("vm_password", None)
        if vm_password:
            salt = bcrypt.gensalt()
            config_vars["vm_hash"] = bcrypt.hashpw(
                vm_password.encode(),
                salt
            ).decode()

    def start_server(self):
        if not self.config["serve_dir"]:
            self.logger.warning(
                "Refusing to start server because serve_dir is missing")
            return False

        self.logger.warning(
            "Serving {} on {}:{}".format(
                self.config["serve_dir"],
                self.config["serve_ip"],
                self.config["serve_port"],
            )
        )

        serve_vars = {}
        for var_file in self.serve_vars:
            conf = utils.load_config(
                var_file,
                disable_jinja=self.disable_jinja,
                jinja_funcs=self.jinja_funcs,
                overrides=self.overrides
            )
            serve_vars.update(conf)

        self.insert_hash(serve_vars)

        server_logger = utils.get_logger(
            name=f"{self.__class__.__name__}.file_server",
            level=self.config["log_level"],
        )
        self.file_server = utils.get_file_server(
            self.config["serve_dir"],
            self.config["serve_ip"],
            self.config["serve_port"],
            user_vars=serve_vars,
            logger=server_logger
        )

        if self.pool is None:
            # Allow start_server to be called directly for debugging
            self.logger.info("Running file_server in the foreground")
            self.file_server.serve_forever()
        else:
            self.workers["file_server"] = utils.spawn(
                self.file_server.serve_forever)

    def stop_server(self):
        self.file_server.stop()
        utils.kill(self.workers["file_server"])

    def run_packer(self, packer_config, user_vars=None, var_file=None, env=None, logger=None, force=False):
        if not logger:
            loggger = self.logger

        var_files = []

        if self.serve_vars:
            var_files.extend(self.serve_vars)

        if self.packer_vars:
            var_files.extend(self.packer_vars)

        if var_file:
            var_files.append(var_file)

        if self.packer_config:
            self.logger.warning(
                "Overriding packer config with {}".format(self.packer_config))
            packer_config = self.packer_config

        if not user_vars:
            user_vars = {}

        # Pass any packinator config options to packer on commandline
        #   (Not passed as a file to allow overrides)
        for key, val in self.config.items():
            if not key in user_vars:
                user_vars[key] = val
        
        # Give packer the base URL of the file server, so it can be passed to scripts
        user_vars["file_server"] = self.get_server_url()

        cmd = utils.get_packer_command(
            packer_config, var_files, user_vars, force=force, on_error=self.on_error)

        self.logger.debug(f"Starting subprocess: {cmd}")

        return utils.run_with_logger(self.proc_list, cmd, logger, env)
