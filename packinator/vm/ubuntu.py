#!/usr/bin/env python3
import os

from packinator.vm import VM
import packinator.utils as utils


class Ubuntu(VM):
    """
    Ubuntu

    https://ubuntu.com/
    """

    def get_customization_type(self):
        # Ubuntu supports standard linux customization
        return "linux"

    def preseed_boot_command(self, prefix=""):
        """
        Tested on Ubuntu 12 - 19
        """
        cmd = ""

        # Choose English install
        cmd += "<enter><wait>"
        # F6: Advanced / Esc: Boot line
        cmd += "<f6><wait><esc><wait>"

        # If this boot line is non-standard, fix it
        cmd += prefix

        # Append to the boot line
        #"boot=casper initrd=/casper/initrd quiet --- "
        cmd += " ".join(
            [
                "auto",
                "priority=critical",
                "locale=en_US.UTF-8",
                "url={}".format(self.config['unattended_cfg']),
            ]
        )

        # Boot
        cmd += "<enter>"
        return cmd

    def get_boot_command(self):
        cmp_ver = utils.get_comparable_version(self.config["os_version"])
        if utils.version_in_range(self.config["os_version"], "14.04.1", "14.04.4"):
            # For some reason, the boot line for 14.04.1-4 ends with 'quiet -- ' instead of 'quiet --- '
            return self.preseed_boot_command("<bs>- ")
        else:
            return self.preseed_boot_command()
