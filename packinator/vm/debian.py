#!/usr/bin/env python3
import os

from packinator.vm import VM
import packinator.utils as utils

class Debian(VM):
    """
    Builds Debian VMs.

    https://www.debian.org/
    """

    def get_customization_type(self):
        # Debian supports standard linux customization
        return "linux"

    def preseed_boot_command(self):
        cmd = "<wait5>"

        # Get to boot line
        cmd += "<wait><esc><wait>"

        # Append to the boot line
        #"boot=casper initrd=/casper/initrd quiet --- "
        cmd += "<wait> ".join(
            [
                "auto",
                "priority=critical",
                "locale=en_US.UTF-8",
                "url={}".format(self.config['unattended_cfg']),
            ]
        )

        # Boot
        cmd += "<enter>"
        return cmd

    def get_boot_command(self):
        cmp_ver = utils.get_comparable_version(self.config["os_version"])
        return self.preseed_boot_command()

    def resolve_paths(self):
        # Get defaults, and overwrite the ones we don't like
        super().resolve_paths()

        # Do not overwrite the cfg if the user explictly set it in the config file
        if "unattended_cfg" not in self.original_keys:
            return

        os_ver = self.config["os_version"]

        preseed = "preseed.cfg"

        # Debian <= 8 needs archive mirror instead
        if utils.version_le(os_ver, "8"):
            preseed = "preseed.archive.cfg"

        # Use Linux/Debian/preseed*.cfg
        self.config["unattended_cfg"] = utils.get_preseed_url(
            self.config["file_url"],
            self.config["os_flavor"],
            filename=preseed
        )
