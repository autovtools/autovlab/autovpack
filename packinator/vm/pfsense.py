#!/usr/bin/env python3
import os
import shutil
import pkg_resources

# Explicit dependency to force import errors if you didn't install terradactsl
import terradactsl.customization.scripts

from packinator.vm import VM
import packinator.utils as utils

class pfSense(VM):
    """
    pfSense doesn't have nice things like preseed / kickstart,
      so instead we do our best and borrow from FreeBSD when we can

    Useful:
        https://www.freebsd.org/cgi/man.cgi?bsdinstall(8)
        https://github.com/boxcutter/bsd/blob/master/http/installerconfig.freebsd
    """
    def get_customization_type(self):
        # If we cannot reliably customize through vSphere API calls
        # https://github.com/vmware/open-vm-tools/issues/236
        # < 2.4.5 has buggy open-vm-tools,
        # so we need guestinfo_agent customization
        if utils.version_lt(self.config["os_version"], "2.4.5"):
            # The customizer is pre-installed on the template
            # (You have to rebuild templates if you change the customizer)
            return "guestinfo_agent"
        else:
            # We can reliably do the customization through API calls
            # (You don't have to rebuild templates if you change the customizer)
            return "guestinfo"

    def _download_guestinfo_agent(self):
        """
        Copy the correct customizer form terradactsl.customization.scripts
            into a tmp directory being served, and download via boot command
        Remember that this code (to generate the boot command) runs before
            we even start packer, so we copy the file we need somewhere
            the booting VM can access it later.
        """
        cmd = ""
        tmp = "tmp"
        tmp_dir = os.path.join(self.packinator.config["serve_dir"], tmp)
        agent_path = pkg_resources.resource_filename(
            "terradactsl.customization.scripts", "pfsense.py"
        )
        # Use the strongest hash as the filename
        agent_hash = utils.compute_hash(agent_path)[-1]
        tmp_file = os.path.join(tmp_dir, agent_hash)
        if not os.path.exists(tmp_file):
            # Slight race condition;
            # if the file gets corrupted, just fix it and try again
            shutil.copyfile(agent_path, tmp_file)

        agent_url = os.path.join(self.packinator.get_server_url(), tmp, agent_hash)
        cmd += utils.waitify(f"fetch -o /tmp/guestinfo_agent.py {agent_url}")
        cmd += "<enter>"
        cmd += "<wait>"*3

        return cmd

    def _get_boot_command_bsdinstall(self):
        """
        Newer bsdinstall-based pfSense.

        1. Click through the installer
        2. Wait for the install to finish (conservatively)
        3. Drop to a manual shell inside the chroot
        4. Get our script + run it
        """

        # Wait for the license screen
        cmd = "<wait10><wait10>"
        # Accept
        cmd += "<enter><wait>"
        # Install
        cmd += "<enter><wait>"
        # Default keymap
        cmd += "<enter><wait>"
        # Auto disk partition
        cmd += "<enter><wait>"

        # Wait for install (paranoid)
        cmd += "<wait10>"*12

        # Enter chroot shell after install 
        cmd += "<left><wait><enter><wait>"

        # Now we are in a shell, start the jank
        # Get a dhcp lease
        cmd += utils.waitify("dhclient vmx0")
        cmd += "<enter><wait10>"
    
        base_url = os.path.dirname(self.config["file_url"])
        conf_dir = "/cf/conf"
        config_url = os.path.join(base_url, "config.xml")
        script_url = os.path.join(base_url, "manual.sh")
        script = "/tmp/manual.sh"

        cmd += utils.waitify(f"fetch -o {conf_dir}/config.xml {config_url}")
        cmd += "<enter><wait>"
        cmd += utils.waitify(f"fetch -o {script} {script_url}")
        cmd += "<enter><wait>"

        if self.get_customization_type() == "guestinfo_agent":
            cmd += self._download_guestinfo_agent()
        
        # Run the install script to prep everything
        cmd += utils.waitify(f"sh {script}")
        cmd += "<enter><wait5>"
        # (the script reboots)

        return cmd

    def _get_boot_command_lua(self):
        """
        Older lua_installer-based pfSense installation

        1: Get to recovery shell
        2: Get our scripts
        3. Run the scripts (which patch the installer)
        4. Run the modified installer + answer questions
        """

        # WARNING: timing-dependent
        # We need to hit a 10s window to enter recovery mode
        #  ~ 25s after boot starts
        cmd = "<wait10><wait10>"
        # Choose recovery mode instead of installer
        cmd += "r<wait>" 

        # Now we are in a shell, start the jank
        # Get a dhcp lease
        cmd += utils.waitify("dhclient vmx0")
        cmd += "<enter><wait10>"

        # Make /tmp/mnt/cf/conf and put config.xml there
        #   (this tricks the installer into "restoring" a custom config.xml)
        # base_url = .../BSD/pfSense
        base_url = os.path.dirname(self.config["file_url"])
        conf_dir = "/tmp/mnt/cf/conf"
        config_url = os.path.join(base_url, "config.xml")
        installer_url = os.path.join(base_url, "install.sh")
        installer = "/tmp/install.sh"
            
        cmd += utils.waitify(f"mkdir -p {conf_dir}")
        cmd += "<enter><wait>"
        cmd += utils.waitify(f"fetch -o {conf_dir}/config.xml {config_url}")
        cmd += "<enter><wait>"
        cmd += utils.waitify(f"fetch -o {installer} {installer_url}")
        cmd += "<enter><wait>"
        if self.get_customization_type() == "guestinfo_agent":
            cmd += self._download_guestinfo_agent()

        # Run the install script to prep everything
        cmd += utils.waitify(f"sh {installer}")
        cmd += "<enter><wait5>"

        # Launch the pfSense installer + wait for the prompt
        cmd += utils.waitify(f"/scripts/lua_installer")
        cmd += "<enter>"
        cmd += "<wait10>"*3
        
        # Wrap around to 'Accept these Settings" (with all defaults)
        cmd += "<up><wait><enter><wait>"
        # Quick / easy install
        cmd += "<enter><wait>"
        # OK (erase warning)
        cmd += "<enter><wait>"

        # Wait for the install to finish
        # Be paranoid or we will accidently cancel
        # (Warning: this is IOPS-bound, build in batches if you hardware struggles)
        cmd += "<wait10>"*36

        # Standard kernel
        cmd += "<enter><wait>"

        # (Wait for after_install_routines.sh, which we patched, to run)
        # Be paranoid or we will accidently cancel
        cmd += "<wait10>"*3

        # Reboot
        cmd += "<enter><wait>"

        # Note: the system will reboot a few times, but eventually packer should find it
        return cmd

    def get_boot_command(self):
        # Old versions of pfSense (< 2.4 ?) use a custom
        #   /scripts/lua_installer instead of bsdinstaller
        # Newer versions (>= 2.4 ?) use bsdinstall, but has lots of logic in
        #   /usr/libexec/bsdinstall/auto, so using 'bsdinstall script installer.cfg' doesn't work well
        #   Thankfully, the install is fast and lets us drop to a shell after,
        #   so we can just step through the normal installation.
        os_ver = self.config["os_version"]
        if utils.version_lt(os_ver, "2.4"):
            return self._get_boot_command_lua() 
        else:
            return self._get_boot_command_bsdinstall() 

    def post_build(self):
        res = True

        # Enable the Ovf Environment.
        # GuestInfo customizer needs it to read MAC addreses in vSphere-order
        # (pfSense likes to scramble the interfaces during deployment)
        res = self.packinator.enable_ovf_env(
            self.config["vm_folder"],
            self.config["vm_name"],
        )

        # Do the prep-template last, or our changes won't be in the snapshot
        res = res and super().post_build()

        return res 
