#!/usr/bin/env python3
import os

from packinator.vm import VM
import packinator.utils as utils

class Kali(VM):
    """
    Kali Linux, a Penetration-Testing Distro.

    https://www.kali.org/docs/
    """
    def get_customization_type(self):
        # Kali supports standard linux customization
        return "linux"

    def get_boot_command(self):
        # Edit boot options
        cmd = "<wait5>"
        cmd += "<esc>"
        # Prepare unattended install
        cmd += " ".join(
            [
                "auto",
                "priority=critical",
                "locale=en_US.UTF-8",
                "keymap=us",
                "url={}".format(self.config["unattended_cfg"])
            ]
        )
        # Boot
        cmd += "<wait>"
        cmd += "<enter>"
        return cmd

    def resolve_paths(self):
        # Get defaults, and overwrite the ones we don't like
        super().resolve_paths()

        # Use a different preseed file for each variant (e.g. rolling, light, full)
        # This will override any option in the kali config
        variant = self.config["os_variant"]
        preseed = "current.cfg"

        # TODO: Add other preseeds, if needed
        if variant.endswith("Rolling"):
            preseed = "rolling.cfg"

        # Use os_flavor (e.g. Kali) preeseed.cfg
        if not "unattended_cfg" in self.original_keys:
            # Do not overwrite the cfg if the user explictly set it in the config file
            self.config["unattended_cfg"] = utils.get_preseed_url(
                self.config["file_url"],
                self.config["os_flavor"],
                filename=preseed
            )

        if not "setup_script" in self.original_keys:
            self.config["setup_script"] = utils.get_setup_script(
                self.config["file_dir"],
                self.config["os_flavor"],
            )

    def apply_overrides(self, user_vars):
        # Lie about os_guest so vSphere thinks it's supported
        #   https://partnerweb.vmware.com/programs/guestOS/guest-os-customization-matrix.pdf
        if self.config["os_arch"] == "x86":
            user_vars["os_guest"] = "ubuntuGuest"
        else:
            user_vars["os_guest"] = "ubuntu64Guest"
        return user_vars

