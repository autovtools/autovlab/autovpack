#!/usr/bin/env python3
import os

from packinator.vm import VM
import packinator.utils as utils

class CoreOS(VM):
    """
    Fedora CoreOS
    
    https://docs.fedoraproject.org/en-US/fedora-coreos/
    """
    def get_customization_type(self):
        # Fedora supports standard linux customization
        return "linux"

    def get_boot_command(self):
        """
        curl -LO https://example.com/example.ign
        sudo coreos-installer install /dev/sda --ignition-file example.ign
        """
        # Live-boot to bash
        cmd = "<wait10>"*3
        ign_url = self.config["unattended_cfg"]
        ign_file = os.path.basename(ign_url)
        cmd += utils.waitify(f"curl -LO {ign_url}")
        cmd += "<enter>"
        cmd += "<wait5>"
        # DEBUG
        cmd += utils.waitify(
            f"sudo coreos-installer install /dev/sda --ignition-file {ign_file}" \
            " && sudo reboot"
        )
        cmd += "<wait><enter>"
        return cmd

    def resolve_paths(self):
        # Get defaults, and overwrite the ones we don't like
        super().resolve_paths()

        # Use os_flavor (e.g. CoreOS) ignition files
        if "unattended_cfg" not in self.original_keys:
            # Do not overwrite the cfg if the user explictly set it in the config file
            self.config["unattended_cfg"] = utils.get_preseed_url(
                self.config["file_url"],
                self.config["os_flavor"],
                filename="conf.ign"
            )
        if "setup_script" not in self.original_keys:
            # Always use latest/setup.sh
            self.config["setup_script"] = utils.get_setup_script(
                self.config["file_dir"],
                self.config["os_flavor"],
            )

    def apply_overrides(self, user_vars):
        # Lie about os_guest so vSphere thinks it's supported
        #   https://partnerweb.vmware.com/programs/guestOS/guest-os-customization-matrix.pdf
        if self.config["os_arch"] == "x86":
            user_vars["os_guest"] = "centos7Guest"
        else:
            user_vars["os_guest"] = "centos8_64Guest"
        return user_vars
