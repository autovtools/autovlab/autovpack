#!/usr/bin/env python3
import os

from packinator.vm import VM
import packinator.utils as utils


class CentOS(VM):

    def get_customization_type(self):
        # CentOS supports standard linux customization
        return "linux"

    def _get_boot_command_5(self):
        cmd = "<wait5>"
        # Edit boot options, then type linux (there is no existing boot line)
        cmd += "<tab><wait>linux "

        centos_url = utils.get_centos_repo(
            self.config["os_version"],
            self.config["os_arch"]
        )
        # 5 doesn't need epel, since we install vmware tools directly from vmware
        cmd += "<wait> <wait>".join(
            [
                "noninteractive",
                "ip=dhcp",
                "repo",
                "--name=CentOS",
                "--baseurl={}".format(centos_url),
                "ks={}".format(self.config["unattended_cfg"])
            ]
        )

        # DEBUG
        cmd += "<wait5>"
        cmd += "<enter><wait>"
        return cmd

    def _get_boot_command_6(self):
        cmd = "<wait5>"
        # Edit boot options, then type a space
        cmd += "<tab><wait> <wait>"

        cmd += "<wait> <wait>".join(
            [
                "noninteractive",
                "ip=dhcp",
                "repo={}".format(
                    utils.get_centos_repo(
                        self.config["os_version"],
                        self.config["os_arch"]
                    )
                ),
                "repo",
                "--name=epel",
                "--baseurl={}".format(
                    utils.get_epel_repo(
                        self.config["os_version"],
                        self.config["os_arch"]
                    )
                ),
                "ks={}".format(self.config["unattended_cfg"])
            ]
        )

        # DEBUG
        cmd += "<wait5>"
        cmd += "<enter><wait>"
        return cmd

    def _get_boot_command_7(self):
        cmd = "<wait5>"
        # Edit boot options, then type a space
        cmd += "<tab><wait> <wait>"

        cmd += "<wait> <wait>".join(
            [
                "noninteractive",
                "ip=dhcp",
                "inst.repo={}".format(
                    utils.get_centos_repo(
                        self.config["os_version"],
                        self.config["os_arch"]
                    )
                ),
                "ks={}".format(self.config["unattended_cfg"])
            ]
        )
        # DEBUG
        cmd += "<wait5>"
        cmd += "<enter><wait>"
        return cmd

    def _get_boot_command_8(self):
        cmd = "<wait5>"
        # Edit boot options, then type a space
        cmd += "<tab><wait> <wait>"

        cmd += "<wait> <wait>".join(
            [
                "noninteractive",
                "ip=dhcp",
                "repo={}".format(
                    utils.get_centos_repo(
                        self.config["os_version"],
                        self.config["os_arch"]
                    )
                ),
                "ks={}".format(self.config["unattended_cfg"])
            ]
        )
        # DEBUG
        cmd += "<wait5>"
        cmd += "<enter><wait>"
        return cmd

    def get_boot_command(self):
        ver = self.config["os_version"]
        if ver.startswith("5."):
            return self._get_boot_command_5()
        if ver.startswith("6."):
            return self._get_boot_command_6()
        elif ver.startswith("7."):
            return self._get_boot_command_7()
        elif ver.startswith("8."):
            return self._get_boot_command_8()
        else:
            self.logger.warning(
                f"boot_command for CentOS {ver} may not be fully supported!")
            return self._get_boot_command_8()

    def resolve_paths(self):
        # Get defaults, and overwrite the ones we don't like
        super().resolve_paths()

        os_ver = self.config["os_version"]

        # Installing the desktop in the kickstart is the most reliable
        variant = self.config["os_variant"]
        preseed = "server.cfg"

        # CentOS 5-6 use the same kickstart file and just install the desktop later
        if utils.version_ge(os_ver, "7"):
            if variant.startswith(("Desktop", "Dev")):
                # User wants a GUI; use the GUI kickstart
                preseed = "desktop.cfg"
        elif utils.version_lt(os_ver, "6"):
            # CentOS 5 needs a different kickstart file for each arch
            preseed = f"server.{self.config['os_arch']}.cfg"

        # Do not overwrite the cfg if the user explictly set it in the config file
        if "unattended_cfg" not in self.original_keys:
            # Use os_version (e.g. CentOS X.X.X ) kickstart files
            #  (kickstart format changes with each major release)
            self.config["unattended_cfg"] = utils.get_preseed_url(
                self.config["file_url"],
                self.config["os_version"],
                filename=preseed
            )
        if utils.version_lt(os_ver, "6") and "packer_config" not in self.original_keys:
            # We need a legacy packer config (VMware drivers missing)
            self.config["packer_config"] = utils.get_packer_config(
                self.config["file_dir"],
                self.config["os_family"],
                filename="legacy-packer.json"
            )

    def post_build(self):
        res = True

        if utils.version_lt(self.config["os_version"], "6"):
            # The VM has generic hardware that we can now upgrade to vmware hardware
            res = self.packinator.enable_vmwarehardware(
                self.config["vm_folder"],
                self.config["vm_name"],
            )

        # Do the prep-template last, or our changes won't be in the snapshot
        res = res and super().post_build()

        return res
