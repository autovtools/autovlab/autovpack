"""
Module containg VM object definitions for use by Packinator
"""
from .vm import VM
from .ubuntu import Ubuntu
from .kali import Kali
from .centos import CentOS
from .debian import Debian
from .fedora import Fedora
from .coreos import CoreOS
from .opensuse import openSUSE
from .freebsd import FreeBSD
from .pfsense import pfSense
