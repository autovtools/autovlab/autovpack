#!/usr/bin/env python3
import os

from packinator.vm import VM
import packinator.utils as utils


class Fedora(VM):
    """
    Fedora is meant to be bleeding-edge, so only modern releases are supported.

    (If you need an old, rpm-based distro, try CentOS)
    """
    def get_customization_type(self):
        # Fedora supports standard linux customization
        return "linux"

    def get_boot_command(self):
        cmd = "<wait5>"
        # Edit boot options, then type a space
        cmd += "<tab><wait> <wait>"

        cmd += "<wait> <wait>".join(
            [
                "ip=dhcp",
                "inst.repo={}".format(
                    "/<wait>".join(
                        utils.get_fedora_repo(
                            self.config["os_version"],
                            self.config["os_arch"]
                        ).split("/")
                    )
                ),
                "ks={}".format(self.config["unattended_cfg"])
            ]
        )

        cmd += "<enter>"
        return cmd

    def resolve_paths(self):
        # Get defaults, and overwrite the ones we don't like
        super().resolve_paths()

        os_ver = self.config["os_version"]

        # The most reliable way to get a functional desktop install is to do it in the kickstart
        #   (instead of installing a GUI in the setup script)
        variant = self.config["os_variant"]
        preseed = "server.cfg"

        if variant.startswith(("Desktop", "Dev")):
            # Use the GUI kickstart
            preseed = "desktop.cfg"

        # Use os_flavor (e.g. Fedora) kickstart files
        if "unattended_cfg" not in self.original_keys:
            # Do not overwrite the cfg if the user explictly set it in the config file
            self.config["unattended_cfg"] = utils.get_preseed_url(
                self.config["file_url"],
                self.config["os_flavor"],
                filename=preseed
            )

    def apply_overrides(self, user_vars):
        # Lie about os_guest so vSphere thinks it's supported
        #   https://partnerweb.vmware.com/programs/guestOS/guest-os-customization-matrix.pdf
        if self.config["os_arch"] == "x86":
            user_vars["os_guest"] = "centos7Guest"
        else:
            user_vars["os_guest"] = "centos8_64Guest"
        return user_vars
