#!/usr/bin/env python3
import os

from packinator.vm import VM
import packinator.utils as utils


class openSUSE(VM):
    """
    openSUSE

    https://www.opensuse.org/
    """
    def get_customization_type(self):
        # openSUSE supports standard linux customization
        return "linux"

    def get_boot_command(self):
        # openSUSE can't handle normal-speed keystrokes, so slow it wayyy down
        cmd = "<wait5><down><wait>"

        cmd += "<wait> ".join(
            [
                utils.waitify("netsetup=dhcp", "="),
                utils.waitify("install={}".format(
                        utils.get_opensuse_repo(
                            self.config["os_version"],
                            self.config["os_arch"]
                        )
                    )
                , "="),
                utils.waitify(f"autoyast={self.config['unattended_cfg']}", None)
            ]
        )
        cmd += "<enter><wait>"
        return cmd

    def resolve_paths(self):
        # Get defaults, and overwrite the ones we don't like
        super().resolve_paths()

        os_ver = self.config["os_version"]

        variant = self.config["os_variant"]
        preseed = "autoinst.xml"

        if variant.startswith(("Desktop", "Dev")):
            pass
            # Use the GUI kickstart
            #preseed = "desktop.cfg"

        # Use os_flavor (e.g. openSUSE) kickstart files
        if "unattended_cfg" not in self.original_keys:
            # Do not overwrite the cfg if the user explictly set it in the config file
            self.config["unattended_cfg"] = utils.get_preseed_url(
                self.config["file_url"],
                self.config["os_flavor"],
                filename=preseed
            )
