#!/usr/bin/env python3
import os
import tempfile
import logging

from packinator.opts import VM_OPTS
import packinator.utils as utils


class VM():
    """
    Abstract VM object / interface for use by Packinator
    """
    opts = VM_OPTS

    def __init__(self, config, packinator, log_level=None, log_file=None):
        self.var_file = None
        # List of keys in self.config that were computed dynamically
        #   and should be passed to Packer as arguments
        self.original_keys = []
        self.env = {}

        if log_level is None:
            log_level = packinator.get_log_level()
        self.logger = utils.get_logger(
            self.__class__.__name__,
            level=log_level,
        )
        self.packinator = packinator
        if isinstance(config, str):
            # We got a filename to a config file
            self.var_file = os.path.abspath(config)
            self.logger.info(f"Loading {config}...")
            config = utils.load_config(config)
            self.original_keys = list(config.keys())

        self.config = config

        if config.get("log_level", None):
            log_level = logging.getLevelName(config["log_level"])
            self.logger.setLevel(log_level)

        self.config["log_level"] = log_level

        self.validate_opts(config)
        self.compute_fields()

        if self.config["customization_type"] is None:
            # User wants the default customzation_type for this class
            self.config["customization_type"] = self.get_customization_type()

        # Switch to a VM-specific logger
        self.logger = utils.get_logger(
            self.config["vm_name"],
            level=log_level,
            log_file=log_file
        )

        self.logger.debug("VM.config: ")
        if self.logger.getEffectiveLevel() <= logging.DEBUG:
            utils.pprint_config(self.config)

    def compute_fields(self):
        """
        Sets all computed config fields so they can be used elsewhere
        """
        self.config["vm_name"] = self.packinator.get_vm_name(
            self.config["os_family"],
            self.config["os_flavor"],
            self.config.get("os_variant", None),
            self.config.get("os_version", None),
            self.config.get("os_arch", None),
        )

        self.config["vm_folder"] = self.packinator.get_vm_folder(
            self.config["os_family"],
            self.config["os_flavor"],
            self.config.get("os_variant", None),
            self.config.get("os_version", None),
            self.config.get("os_arch", None),
        )

        self.config["vm_path"] = '/'.join(
            [
                self.config["vm_folder"],
                self.config["vm_name"],
            ]
        )
        if not self.config.get("iso_name", None):
            # Warning: won't work if URL has ?params
            self.config["iso_name"] = os.path.basename(
                self.config["iso_url"]
            )

        self.config["iso_path"] = self.packinator.get_iso_path(
            self.config["os_family"],
            self.config["os_flavor"],
            iso_name=self.config["iso_name"]
        )

        self.set_file_url()
        self.set_file_dir()

        self.resolve_paths()

        boot_command = None
        try:
            boot_command = self.get_boot_command()
        except NotImplementedError:
            pass
        self.config["boot_command"] = boot_command
        if boot_command:
            self.env["boot_command"] = boot_command

    def resolve_paths(self):
        """
        Compute any last-minute config values (e.g. file paths / urls).

        Ex: Compute the path to preeseed.cfg and safe it in self.config so 
            they can be used in get_boot_command.
        """
        self.logger.debug("Resolving paths (VM)")
        if not self.config.get('helpers_src', None):
            # Use os_family (e.g. Linux) helpers.sh
            self.config["helpers_src"] = utils.get_os_helpers(
                self.config["file_dir"],
                self.config["os_family"],
            )

        if not self.config.get('setup_script', None):
            # Use os_version (e.g. 18.04.X) setup.sh
            self.config["setup_script"] = utils.get_setup_script(
                self.config["file_dir"],
                self.config["os_version"],
            )

        if not self.config.get('packer_config', None):
            # Use os_family (e.g. Linux) packer.json
            self.config["packer_config"] = utils.get_packer_config(
                self.config["file_dir"],
                self.config["os_family"],
            )

        if not self.config.get('unattended_cfg', None):
            # Use os_flavor (e.g. Ubuntu) preeseed.cfg
            self.config["unattended_cfg"] = utils.get_preseed_url(
                self.config["file_url"],
                self.config["os_flavor"],
            )

    def set_file_url(self):
        """
        Set the value of file_url to the url where this VM's config files are stored.

        Default: Assume all files are in files/<os_family>/<os_flavor>/<os_version>
            Override this method if your VMs files are broken down differently
        """
        self.config["file_url"] = self.packinator.get_file_url(
            self.config["os_family"],
            self.config["os_flavor"],
            os_version=self.config["os_version"]
        )

    def set_file_dir(self):
        """
        Set the value of file_dir to the local path to where this VM's config files are stored.

        Default: Assume all files are in files/<os_family>/<os_flavor>/<os_version>
            Override this method if your VMs files are broken down differently
        """
        self.config["file_dir"] = self.packinator.get_file_dir(
            self.config["os_family"],
            self.config["os_flavor"],
            os_version=self.config["os_version"]
        )

    def get_customization_type(self):
        """
        Returns the "customization_type" to use for this VM class.

        Override in a subclass of VM to change behavior.
        Acceptable return values:
            linux
            windows
            guestinfo
            none 
        """
        # default to no customization so new VM types
        #   can still be deployed to DHCP networks
        return "none"

    def get_boot_command(self):
        raise NotImplementedError()

    def validate_opts(self, config):
        """
        Performs any necessary validation for the provided config.

        If extra validation logic is necessary, override this.
        """
        utils.validate_opts(self.opts, config)

    def iso_exists(self):
        return self.packinator.iso_exists(
            self.config["iso_name"],
            self.config["os_family"],
            self.config["os_flavor"]
        )

    def ensure_iso(self):
        exists, path = self.iso_exists()
        if exists:
            self.logger.info(f"Using existing ISO at {path}")
            return True
        self.logger.info("Downloading ISO from {}".format(
            self.config["iso_url"]))
        with tempfile.NamedTemporaryFile() as tmp_file:
            utils.download_file(self.config["iso_url"], out_file=tmp_file)
            tmp_file.flush()
            expected = self.config.get("iso_checksum", None)
            # If jinja emitted a None
            if expected == "None" or expected == "nil":
                expected = None
            if expected:
                # User gave us a hash, so We need to verify
                verified, actual = utils.verify_file(tmp_file.name, expected)

                if not verified:
                    msg = f"ISO failed checksum: {actual} vs {expected}"
                    self.logger.error(msg)
                    with open(tmp_file.name, "rb") as failed_iso:
                        self.logger.debug(failed_iso.read(1024))
                    raise Exception(msg)
                else:
                    self.logger.info(f"Verified ISO with hashes: {actual}")

            self.packinator.upload_iso(
                tmp_file.name,
                self.config["os_family"],
                self.config["os_flavor"],
                upload_name=path
            )

    def get_user_vars(self):
        return {
            key: value for key, value in self.config.items()
            if not key in self.original_keys
            and not key in self.env
        }

    def build(self, force=False, ensure_iso=True):
        exists = self.packinator.vm_exists(
            self.config["vm_folder"],
            self.config["vm_name"],
        )
        vm_name = self.config["vm_name"]
        if exists:
            self.logger.warn(f"VM ({vm_name}) already exists!")
            if force:
                self.logger.warn(f"VM ({vm_name}) Will be deleted!")
                # It's probably safer to pass the -force flag to packer
                # self.packinator.remove_vm(
                #    self.config["vm_folder"],
                #    self.config["vm_name"]
                # )
            else:
                return True
        else:
            # There is no need to force
            force = False

        if ensure_iso:
            self.ensure_iso()

        user_vars = self.get_user_vars()
        self.logger.debug("user_vars:")
        if self.logger.getEffectiveLevel() <= logging.DEBUG:
            utils.pprint_config(user_vars)

        self.logger.debug("Starting packer...")
        user_vars = self.apply_overrides(user_vars)
        if not self.packinator.run_packer(
            self.config["packer_config"],
            user_vars=user_vars,
            var_file=self.var_file,
            env=self.env,
            logger=self.logger,
            force=force
        ):
            # Abort if packer failed / was killed
            return False

        self.post_build()

        self.logger.debug("\nBuild complete: ")
        if self.logger.getEffectiveLevel() <= logging.DEBUG:
            utils.pprint_config(self.config)

    def post_build(self):
        return self.packinator.prep_template(
            self.config["vm_folder"],
            self.config["vm_name"],
            self.config
        )

    def apply_overrides(self, user_vars):
        return user_vars
