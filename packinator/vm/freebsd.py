#!/usr/bin/env python3
import os

from packinator.vm import VM
import packinator.utils as utils

class FreeBSD(VM):
    """
    FreeBSD doesn't have nice things like preseed / kickstart,
      so instead we do a commandline install using setup scripts.

    Useful:
        https://www.freebsd.org/cgi/man.cgi?bsdinstall(8)
        https://github.com/boxcutter/bsd/blob/master/http/installerconfig.freebsd
    """
    def get_customization_type(self):
        # FreeBSD needs guestinfo customizaiton
        return "guestinfo"

    def get_boot_command(self):
        """
        1: Get to shell
        2: Get our scripts
        3. Run the scripts
        """
        # Wait for boot menu
        cmd = "<wait10>"*3
        # Choose "Shell"
        cmd += "<right><wait><enter><wait>"

        # Get a dhcp lease
        # /etc/resolv.conf is on the read-only CD and is a symlink to
        #   /tmp/bsdinstall_etc/resolv.conf
        # Make that directory, so that when we get a lease,
        #the write succeeds and we get DNS
        cmd += utils.waitify("mkdir -p /tmp/bsdinstall_etc")
        cmd += "<enter><wait>"
        cmd += utils.waitify("dhclient vmx0")
        cmd += "<enter><wait10>"
    
        # Download files
        # base_url = .../BSD/FreeBSD
        base_url = os.path.dirname(self.config["file_url"])
        
        for url in utils.get_freebsd_files(base_url):
            filename = os.path.basename(url)
            cmd += utils.waitify(
                f"fetch -o /tmp/{filename} {url}" + "<enter><wait5>"
            )
        # Set up environment variables specific to this version / arch
        cmd += utils.waitify(
            utils.get_freebsd_environment(
                self.config["os_version"],
                self.config["os_arch"]
            )
        )
        cmd += "<enter><wait>"
        
        # Make our script executable + run it
        cmd += utils.waitify("chmod +x /tmp/install.sh; /tmp/install.sh")

        cmd += "<enter>"

        return cmd
