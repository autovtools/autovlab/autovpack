#!/bin/bash

# Usage:
# cd /path/to/packinator
# ./setup.sh

# Install packer
sudo apt install packer -y
# Install python + venv
sudo apt install -y python3 python3-pip python3-venv

# Debian requires a special .jigdo download client:
#   https://github.com/dajobe/jigdo/blob/master/scripts/jigdo-lite
sudo apt install -y jigdo-file

# Optional: use venv to keep python deps clean / manageable
#   https://docs.python.org/3/library/venv.html
mkdir -p ~/.venv/
python3 -m venv ~/.venv/automation

ACTIVATE="${HOME}/.venv/automation/bin/activate"
# Activate our virtual environment on login
echo ". ${ACTIVATE}" >> ~/.bashrc
# Also activate now (for the rest of the script only)
. "${ACTIVATE}"

# Upgrade pip
pip3 install --upgrade pip
pip3 install wheel

# Install packinator and most python dependenices
pip3 install .
# Uninstall with: pip3 uninstall packinator

# Give user instructions so they can't miss them
cat <<EOF 
WARNING: you also need to manually install:
    terrashell (the python module) from py-terrashell
    terrashell (the powershell scripts) from terrashell

You should edit / create the following files:
packinator.json
    (Edit example file)

/somewhere/safe/vm_secrets.json
{
    "vm_user":"my_vm_user",
    "vm_password":"my_vm_password",
    "ssh_user":"my_vm_user",
    "ssh_password":"my_vm_password"
}

/somewhere/safe/infra_secrets.json
{
    "vsphere_user":"my_automation_user",
    "vsphere_password":"my_automation_password"
}


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Protect your secrets and never commit them to git !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

You will also need to create vSphere directory structures similar to the ones under files/
Ex. A VM folder 'MyDC/Templates/Linux/CentOS'
    and an ISO folder 'MyDataStore/.ISO/Linux/CentOS'

Also, install the latest packer from:
    https://packer.io/downloads.html
EOF
