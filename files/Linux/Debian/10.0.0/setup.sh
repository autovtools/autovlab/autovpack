#!/bin/bash
OS_VARIANT=$1
HOSTNAME=$2
FILE_SERVER=$3
shift
shift
shift

helper_scripts=$@
function finish {
    echo "Removing ${helper_scripts} $0"
    rm -f ${helper_scripts} $0
}
trap finish EXIT
for helper in $@; do 
    echo "Loading ${helper}"
    . "${helper}" --source-only
done

# Needed for bootstrap functions
apt_install curl

# Use the "git" method to bootstrap salt, since there are no i386 packages
# -x python3 required for Debian 10 https://github.com/saltstack/salt-bootstrap/issues/1353
bootstrap_salt_minion "${FILE_SERVER}" -x python3 git

# The generated service files point to the wrong salt-minion path
ln -s "$(which salt-minion)" /usr/bin/salt-minion

set_hostname "${HOSTNAME}"
configure_minion

if [[ "${OS_VARIANT}" =~ ^Desktop ]] || [[ "${OS_VARIANT}" =~ ^Dev ]]; then
    EXTRA_PACKAGES+=" open-vm-tools-desktop"
    apt_install aptitude tasksel
    apt-get update
    # gnome-desktop fails on Debian 8, so just use xfce
    DEBIAN_FRONTEND="noninteractive" tasksel install xfce-desktop --new-install
fi
if [[ "${OS_VARIANT}" =~ ^Dev ]]; then
    EXTRA_PACKAGES+=" build-essential vim curl"
fi

if [ "${EXTRA_PACKAGES}" != "" ]; then
    echo "Installing: ${EXTRA_PACKAGES}"
    apt_install ${EXTRA_PACKAGES}
fi

apt_clean
exit 0
