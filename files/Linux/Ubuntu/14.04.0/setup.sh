#!/bin/bash
OS_VARIANT=$1
HOSTNAME=$2
FILE_SERVER=$3
shift
shift
shift

helper_scripts=$@
function finish {
    echo "Removing ${helper_scripts} $0"
    rm -f ${helper_scripts} $0
}
trap finish EXIT
for helper in $@; do 
    echo "Loading ${helper}"
    . "${helper}" --source-only
done

apt_install curl

# Use the "git" method to bootstrap salt, since there are no i386 packages
bootstrap_salt_minion "${FILE_SERVER}" "git"

set_hostname "${HOSTNAME}"
configure_minion

if [ "${OS_VARIANT}" = "Desktop" ] || [ "${OS_VARIANT}" = "Dev" ]; then
    # 16.X needs xserver-xorg-input-all or you can't type
    EXTRA_PACKAGES+=" ubuntu-desktop open-vm-tools-desktop xserver-xorg-input-all"
fi
if [ "${OS_VARIANT}" = "Dev" ]; then
    EXTRA_PACKAGES+=" build-essential vim"
fi

if [ "${EXTRA_PACKAGES}" != "" ]; then
    echo "Installing: ${EXTRA_PACKAGES}"
    apt_install ${EXTRA_PACKAGES}
fi

apt_clean
exit 0
