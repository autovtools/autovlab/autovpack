#!/bin/bash
OS_VARIANT=$1
HOSTNAME=$2
FILE_SERVER=$3
shift
shift
shift

helper_scripts=$@
function finish {
    echo "Removing ${helper_scripts} $0"
    rm -f ${helper_scripts} $0
}
trap finish EXIT
for helper in $@; do 
    echo "Loading ${helper}"
    . "${helper}" --source-only
done

if [[ "${OS_VARIANT}" =~ .*Prod ]]; then
    # This template is for running infrastructure, apply updates
    apt_full_upgrade
fi

SALT_PACKAGES="salt-minion salt-master salt-ssh salt-syndic"

apt_install ${SALT_PACKAGES}

set_hostname "${HOSTNAME}"
configure_minion

apt_install "cloud-init"
configure_cloud_init

if [[ "${OS_VARIANT}" =~ ^Desktop ]] || [[ "${OS_VARIANT}" =~ ^Dev ]]; then
    EXTRA_PACKAGES+=" ubuntu-desktop open-vm-tools-desktop"
fi
if [[ "${OS_VARIANT}" =~ ^Dev ]]; then
    EXTRA_PACKAGES+=" build-essential vim curl"
fi

if [ "${EXTRA_PACKAGES}" != "" ]; then
    echo "Installing: ${EXTRA_PACKAGES}"
    apt_install ${EXTRA_PACKAGES}
fi

configure_gnome

apt_clean
exit 0
