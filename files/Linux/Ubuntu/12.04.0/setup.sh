#!/bin/bash
OS_VARIANT=$1
HOSTNAME=$2
FILE_SERVER=$3
shift
shift
shift

helper_scripts=$@
function finish {
    echo "Removing ${helper_scripts} $0"
    rm -f ${helper_scripts} $0
}
trap finish EXIT
for helper in $@; do 
    echo "Loading ${helper}"
    . "${helper}" --source-only
done

# 12.04 only has amd64 salt + is too old for bootstrap
SALT_REPO="deb https://repo.saltstack.com/apt/ubuntu/12.04/amd64/latest/ precise main"
SALT_KEY='http://repo.saltstack.com/apt/ubuntu/12.04/amd64/latest/SALTSTACK-GPG-KEY.pub'
SALT_PACKAGES="salt-minion salt-master salt-ssh salt-syndic"

install_third_party "saltstack" "${SALT_REPO}" "${SALT_KEY}" "${SALT_PACKAGES}" "apt-get"


set_hostname "${HOSTNAME}"
configure_minion

if [ "${OS_VARIANT}" = "Desktop" ] || [ "${OS_VARIANT}" = "Dev" ]; then
    # There is no open-vm-tools-desktop package for < 14
    #   screen doesn't resize, but can be set to a reasonable resolution
    EXTRA_PACKAGES+=" ubuntu-desktop"
fi
if [ "${OS_VARIANT}" = "Dev" ]; then
    EXTRA_PACKAGES+=" build-essential curl vim"
fi

if [ "${EXTRA_PACKAGES}" != "" ]; then
    echo "Installing: ${EXTRA_PACKAGES}"
    apt_install ${EXTRA_PACKAGES}
fi

apt_clean
exit 0
