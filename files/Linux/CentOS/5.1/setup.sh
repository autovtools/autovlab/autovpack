#!/bin/bash
OS_VARIANT=$1
HOSTNAME=$2
FILE_SERVER=$3
shift
shift
shift

helper_scripts=$@
function finish {
    echo "Removing ${helper_scripts} $0"
    rm -f ${helper_scripts} $0
}
trap finish EXIT
for helper in $@; do 
    echo "Loading ${helper}"
    . "${helper}" --source-only
done

# Because we don't have sudo -E, we lost some important directories from our PATH
#  (e.g. chkconfig)
export PATH=$PATH:/sbin:/usr/sbin

set_hostname "${HOSTNAME}" 
SALT_RPM_NAME="salt-repo-latest-1.el5.noarch.rpm"
SALT_RPM="http://repo.saltstack.com/yum/redhat/${SALT_RPM_NAME}"
SALT_PACKAGES="salt-minion salt-master salt-ssh salt-syndic"

wget "${SALT_RPM}"
rpm -Uvh "${SALT_RPM_NAME}"
rm -f "${SALT_RPM_NAME}"

# Downgrade to prevent SSL errors
sed -i 's/https:/http:/' /etc/yum.repos.d/salt-latest.repo
yum clean all

yum_install ${SALT_PACKAGES}
configure_minion

# Install cloud-init for GuestOS customization
# (required EPEL)
# CentOS < 5.4 only - cloud-init needs e4fsprogs and rsyslog,
#   which don't exist yet
VER="$(grep baseurl=http /etc/yum.repos.d/CentOS-Base.repo  | awk -F '/' '{print $4}' | head -n 1)"
if [ "${VER}" = "5.0" ] || [ "${VER}" = "5.1" ] || [ "${VER}" = "5.2" ] || [ "${VER}" = "5.3" ]; then
    echo "CentOS ${VER} is missing cloud-init dependencies; adding CentOS-Future.repo"
    BASE="/etc/yum.repos.d/CentOS-Base.repo"
    FUTURE="/etc/yum.repos.d/CentOS-Future.repo"
    # Get a CentOS 5.4 repo file
    cp "${BASE}" "${FUTURE}"
    mv "${BASE}" "${BASE}.tmp"

    sed -i "s/${VER}/5.4/g" "${FUTURE}"
    yum clean all
    yum_install "cloud-init"
    # Remove 'Future' repo
    rm -f "${FUTURE}"
    # Restore original
    mv "${BASE}.tmp" "${BASE}"
    yum clean all
else 
    yum_install "cloud-init"
fi

configure_cloud_init

# Sendmail hangs on boot trying to DNS resolve itself
service_disable sendmail


if [ "${OS_VARIANT}" = "Desktop" ] || [ "${OS_VARIANT}" = "Dev" ]; then
    # Install + enable the GUI
    EXTRA_PACKAGES+=" vmware-tools-esx libXfont xorg*vm*"

    # CentOS < 5.3: https://bugs.centos.org/view.php?id=2483
    yum_install yum-utils
    yumdownloader nautilus-sendto
    rpm -Uvh --nodeps nautilus-sendto*.rpm
    rm -f nautilus-sendto*.rpm

    yum_groupinstall "X Window System"
    yum_groupinstall "GNOME Desktop Environment"
    boot_to_desktop
fi
if [ "${OS_VARIANT}" = "Dev" ]; then
    yum_groupinstall "Development Tools"
fi

if [ "${EXTRA_PACKAGES}" != "" ]; then
    echo "Installing: ${EXTRA_PACKAGES}"
    yum_install ${EXTRA_PACKAGES}
fi

# After switching to VMXNET3 and rebooting, DHCP fails without NM
# (Enable without starting so we don't turn off the interface)
/sbin/chkconfig NetworkManager on

configure_gnome
yum_clean
exit 0
