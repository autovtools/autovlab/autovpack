#!/bin/bash
OS_VARIANT=$1
HOSTNAME=$2
FILE_SERVER=$3
shift
shift
shift
helper_scripts=$@
function finish {
    echo "Removing ${helper_scripts} $0"
    rm -f ${helper_scripts} $0
}
trap finish EXIT
for helper in $@; do 
    echo "Loading ${helper}"
    . "${helper}" --source-only
done

if [[ "${OS_VARIANT}" =~ .*Rolling ]]; then
   apt_full_upgrade 
fi

# Required for apt-key
apt_install gnupg

# https://repo.saltstack.com/#tab1-debian
SALT_REPO="deb http://repo.saltstack.com/py3/debian/10/amd64/latest buster main"
SALT_KEY="https://repo.saltstack.com/py3/debian/10/amd64/latest/SALTSTACK-GPG-KEY.pub"
SALT_PACKAGES="salt-minion salt-ssh"

install_third_party "saltstack" "${SALT_REPO}" "${SALT_KEY}" "${SALT_PACKAGES}" "apt"

set_hostname "${HOSTNAME}"
configure_minion

# cloud-init + dependencies so network config works
apt_install network-manager resolvconf cloud-init

# https://github.com/canonical/cloud-init/blob/44039629e539ed48298703028ac8f10ad3c60d6e/cloudinit/distros/debian.py#L47
# cloud-init debian eni writes net info to:
# /etc/network/interfaces.d/50-cloud-init
# But then sources it like:
# source /etc/network/interfaces.d/*.cfg

# Patch cloud-init to name the file 50-cloud-init.cfg
DEBIAN=$(find /usr/lib/python* -name debian.py | grep cloudinit)
sed -i 's|"/etc/network/interfaces.d/50-cloud-init"|"/etc/network/interfaces.d/50-cloud-init.cfg"|' "${DEBIAN}"

#https://github.com/canonical/cloud-init/blob/master/cloudinit/distros/debian.py#L138
# cloud-init just writes to /etc/hostname
# Edit _write_hostname() to set the host name without reboot
# Current indent is 8
WRITE_HOSTNAME="conf.set_hostname(your_hostname)"
#sed -i "/${WRITE_HOSTNAME}/a \        util.subp(['hostnamectl', 'set-hostname', str(your_hostname)])" "${DEBIAN}"
sed -i "/${WRITE_HOSTNAME}/a \        util.subp(['hostname', str(your_hostname)])" "${DEBIAN}"
# Start dbus or hostnamectl will fail
#sed -i "/${WRITE_HOSTNAME}/a \        util.subp(['systemctl', 'start', 'dbus'])" "${DEBIAN}"
# Also add the new hostname to /etc/hosts
# This gets called more than once; so delete any previous artifacts first
sed -i "/${WRITE_HOSTNAME}/a \        util.subp(['sed', '-i', '/127.0.1.1/a 127.0.2.1 {}'.format(your_hostname), '/etc/hosts'])" "${DEBIAN}"
sed -i "/${WRITE_HOSTNAME}/a \        util.subp(['sed', '-i', 's/^127.0.2.1.*//', '/etc/hosts'])" "${DEBIAN}"

configure_cloud_init

# Always install some basic packages
EXTRA_PACKAGES="kali-tools-top10 kali-linux-core"
EXTRA_PACKAGES+=" curl vim"

if [[ "${OS_VARIANT}" =~ ^Core ]]; then
    echo "Headless Core will only have ${EXTRA_PACKAGES}"
else
    # Everything but Core gets a GUI
    apt_install kali-desktop-xfce
    # Install desktop tools (enable copy / paste
    apt_install open-vm-tools-desktop

    # kali-linux-light got removed at some point
    if [[ ! "${OS_VARIANT}" =~ ^Light ]]; then
        # Everything but Core and Light come with default
        EXTRA_PACKAGES+=" kali-linux-default"
    fi
fi

if [[ "${OS_VARIANT}" =~ ^Offsec ]]; then
    # Nice collection of tools used for Offsec courses
    EXTRA_PACKAGES+=" offsec-pwk offsec-awae"
fi

# Basically all pen-test tools
if [[ "${OS_VARIANT}" =~ ^Full ]]; then
    EXTRA_PACKAGES+=" offsec-pwk offsec-awae"
    EXTRA_PACKAGES+=" kali-linux-large"
    EXTRA_PACKAGES+=" kali-tools-information-gathering kali-tools-vulnerability kali-tools-web kali-tools-database kali-tools-passwords"
    EXTRA_PACKAGES+=" kali-tools-wireless kali-tools-reverse-engineering kali-tools-exploitation kali-tools-social-engineering"
    EXTRA_PACKAGES+=" kali-tools-sniffing-spoofing kali-tools-post-exploitation kali-tools-forensics kali-tools-reporting kali-tools-windows-resources"
fi

if [ "${EXTRA_PACKAGES}" != "" ]; then
    echo "Installing: ${EXTRA_PACKAGES}"
    apt_install ${EXTRA_PACKAGES}
fi

# There is a default Kali config file that will break our GuestOS Customization
rm -f /etc/cloud/cloud.cfg.d/*kali.cfg

apt_clean

exit 0

