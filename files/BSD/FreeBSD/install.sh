#!/bin/sh
# dhclient -l /tmp/dhclient.leases -p /tmp/dhclient.pid vmx0
export PARTITIONS=da0
export DISTRIBUTIONS="MANIFEST kernel.txz base.txz"
export BSDINSTALL_DISTDIR=/var/tmp/
#export BSDINSTALL_DISTSITE=ftp://ftp-archive.freebsd.org/pub/FreeBSD-Archive/old-releases/amd64/10.0-RELEASE
#export PACKAGESITE=http://pkg.freebsd.org/FreeBSD:10:amd64/release_0/
#export BSDINSTALL_CONFIGCURRENT=1
mdmfs -s 400m md /var/tmp

# bsdinstall deletes our resolv.conf file (breaking DNS) at the beginning of
#   bsdinstall script
# So, make a copy of bsdinstall in tmp (/ is read-only CD)
#  and apply hacky patches to prevent the deletion of resolv.conf
#  without breaking bsdinstall
cp $(which bsdinstall) /tmp/bsdinstall.sh
cp -r /usr/libexec/bsdinstall /tmp/bsdinstall
sed -i.bak 's|/usr/libexec/bsdinstall|/tmp/bsdinstall|g' /tmp/bsdinstall.sh
sed -i.bak -E 's|(rm -rf \$BSDINSTALL_TMPETC)|#\1|g' /tmp/bsdinstall/script
sed -i.bak -E 's|mkdir (\$BSDINSTALL_TMPETC)|mkdir -p \1|g' /tmp/bsdinstall/script

# DEBUG: make sure we have DNS
ls -alh /etc/resolv.conf 
cat /etc/resolv.conf
sleep 5

# For some reason, the pkg bootstrap doesn't find the new version until after reboot
#  prep a run-once script to install open-vm-tools after the reboot 
cat <<'EOD' > /tmp/bsdinstall_etc/rc.local
#!/bin/sh
# This script runs on first boot, then is deleted
export ASSUME_ALWAYS_YES=yes
# Need to update pkg, or it doesn't work
pkg bootstrap -f
# Oops, that installed a newer version of pkg, but no libc dependencies
# (just use pkg-static instead from now on)
/usr/local/sbin/pkg-static update
/usr/local/sbin/pkg-static install sudo open-vm-tools
# Enable sshd for second boot
sysrc sshd_enable="YES"
# Enable wheel as sudoers by uncommenting the line
sed -i.bak -E 's/^# (%wheel ALL=\(ALL\) ALL)/\1/' /usr/local/etc/sudoers
# self-delete and reboot (with sudo, open-vm-tools, and ssh available)
rm /etc/rc.local
reboot
EOD

/tmp/bsdinstall.sh distfetch
/tmp/bsdinstall.sh script /tmp/installer.cfg
