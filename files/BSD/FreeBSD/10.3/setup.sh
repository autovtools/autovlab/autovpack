#!/bin/sh
OS_VARIANT=$1
HOSTNAME=$2
FILE_SERVER=$3
shift
shift
shift

helper_scripts=$@
finish() {
    echo "Removing ${helper_scripts} $0"
    rm -f ${helper_scripts} $0
}
trap finish EXIT
for helper in $@; do 
    echo "Loading ${helper}"
    . "${helper}" --source-only
done

EXTRA_PACKAGES="vim"

# BSD (sh) OS_VARIANT.endswith("Prod")
#  (Try to delete Prod from end of string, test if the result is different)
if [ "$(echo ${OS_VARIANT} | sed 's/Prod$//')" != "${OS_VARIANT}" ]; then
    # This template is for running infrastructure, apply updates
    pkg_full_upgrade
fi

set_hostname "${HOSTNAME}" 
SALT_PACKAGES="py27-salt"

pkg_install ${SALT_PACKAGES}
configure_minion

if [ "${EXTRA_PACKAGES}" != "" ]; then
    echo "Installing: ${EXTRA_PACKAGES}"
    pkg_install ${EXTRA_PACKAGES}
fi

exit 0

