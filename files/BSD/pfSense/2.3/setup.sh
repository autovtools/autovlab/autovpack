#!/bin/sh
OS_VARIANT=$1
HOSTNAME=$2
FILE_SERVER=$3
shift
shift
shift

helper_scripts=$@
finish() {
    echo "Removing ${helper_scripts} $0"
    rm -f ${helper_scripts} $0
}
trap finish EXIT
for helper in $@; do 
    echo "Loading ${helper}"
    . "${helper}" --source-only
done

EXTRA_PACKAGES=""

# BSD (sh) OS_VARIANT.endswith("Prod")
#  (Try to delete Prod from end of string, test if the result is different)
if [ "$(echo ${OS_VARIANT} | sed 's/Prod$//')" != "${OS_VARIANT}" ]; then
    # This template is for running infrastructure, un-disable pfSense updates
    # (don't actually upgrade all packages or you'll probably break stuff)
    PFSENSE_UPGRADE="/usr/local/libexec/pfSense-upgrade"
    mv "${PFSENSE_UPGRADE}.disabled" "${PFSENSE_UPGRADE}"
    # Attempt a pfSense upgrade
    pfSense-upgrade -y
fi

set_hostname_pfsense "${HOSTNAME}" 
# pfSense >= 2.4.5 uses py37-salt instead of py27-salt
SALT_PACKAGES_TO_TRY="py37-salt py27-salt"
for SALT_PKG in ${SALT_PACKAGES_TO_TRY}; do
    if pkg search "${SALT_PKG}" > /dev/null; then
        echo "Salt found: ${SALT_PKG}"
        pkg_install "${SALT_PKG}"
        break
    fi
    echo "[ERROR] SALT NOT FOUND. Tried: ${SALT_PACKAGES_TO_TRY}"
done
configure_minion_pfsense

if [ "${EXTRA_PACKAGES}" != "" ]; then
    echo "Installing: ${EXTRA_PACKAGES}"
    pkg_install ${EXTRA_PACKAGES}
fi

# Add a symlink to put python in PATH
PY="/usr/local/bin/python"
PY2="${PY}2"
PY2="${PY}2.7"
PY3="${PY}3"
PY37="${PY}3.7"
TRIED="${PY3} ${PY37} ${PY2} ${PY27}"

for py_bin in $PY3 $PY37 $PY2 $PY27; do
    if [ -e $py_bin ]; then
        echo "Python found: ${py_bin}"
        ln -s $py_bin $PY
        break
    fi
    echo "[ERROR] PYTHON NOT FOUND. Tried: ${TRIED}"
done

# Install vmwarte_guestd watchdog
# Helps mitigate https://github.com/vmware/open-vm-tools/issues/236
SVC="vmware-guestd"
PIDFILE="/var/run/vmware_guestd.pid"
echo "Installing watchdog for ${SVC}"

# Goal: the following line persistently insterted into /etc/crontab:
# *       *       *       *       *       root    /bin/ps -p $(cat "/var/run/vmware_guestd.pid") > /dev/null || service vmware-guestd restart
# (If vmtoolsd died, restart it. Check every minute.)
# /etc/ is blown away on boot, so we have to use the "cron" package and edit config.xml
pkg_install pfsense-pkg-Cron
# Insert the item block before the closing </cron> tab
# (Edits config.xml in place to insert an <item> block; special escaping to make BSD sed happy)
sed -i'' -e '\@</cron>@i\
                <item>\
                        <minute>*</minute>\
                        <hour>*</hour>\
                        <mday>*</mday>\
                        <month>*</month>\
                        <wday>*</wday>\
                        <who>root</who>\
                        <command>/bin/ps -p $(cat "'${PIDFILE}'") > /dev/null || service "'${SVC}'" restart</command>\
                </item>\
' /cf/conf/config.xml

exit 0

