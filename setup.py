from setuptools import setup, find_packages
# WARNING: has additional, custom dependencies
#  terrashell (the python module from the py-terrashell project)
#  terrashell (the powershell scripts from the terrashell project)
setup(name='packinator',
      version='0.1',
      packages=find_packages(),
      install_requires=[
        'requests',
        'bcrypt',
        'gevent',
        'flask',
        'jinja2',
        'lxml',
        'termcolor',
        'coloredlogs',
        'terradactsl'
      ],
      include_package_data=True
)
