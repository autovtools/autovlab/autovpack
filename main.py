#!/usr/bin/env python3
from gevent import monkey
monkey.patch_all()

import os
import sys
import json
import re
import logging
import argparse
from pathlib import Path

from packinator import Packinator
from packinator import utils
from packinator.utils import jinja_funcs

def get_args():
    """
    Parse CLI args and return the result.
    """
    parser=argparse.ArgumentParser(
        description='Packinator: A python wrapper around Packer to meet all your packinating needs'
    )
    parser.add_argument(
        '-c', "--config",
        default="./packinator.yml",
        help="The packinator config file (json), shared with all VMs"
    )
    parser.add_argument(
        '-i', "--include",
        action="append",
        help="Extra packer var-files that should be shared"
             " with all VMs **and the file_server**. Repeatable."
             " (Ex. default VM creds)"
    )
    parser.add_argument(
        "--var-file",
        action="append",
        help="Extra packer var-files that should be passed directly"
             " to packer and be unavailable to Packinator. Repeatable."
             " (Ex. infrastructure creds)"
    )
    parser.add_argument(
        'vm_config',
        nargs="*",
        help="A list of VM config files, one for each VM to be built"
    )
    parser.add_argument(
        '-v', '--verbose',
        action="store_true",
        help="Enable debug logging"
    )
    parser.add_argument(
        '-q', '--quiet',
        action="store_true",
        help="Only log errors"
    )
    parser.add_argument(
        '--server-only',
        action="store_true",
        help="Only start file_server (serving forever), skipping all build steps."
    )
    parser.add_argument(
        '--skip-ensure-iso',
        action="store_true",
        help="Skip the build step that ensures ISOs are present. Assumes they" \
             " have already been ingested. Build will fail if they are not."
    )
    parser.add_argument(
        '--skip-build',
        action="store_true",
        help="Skip the build step that creates VMs." \
             " VMs will not be created or destroyed."
    )
    parser.add_argument(
        '--skip-post-build',
        action="store_true",
        help="Skip the post-build step that applies tags, etc." \
             " Note: ignored unless --skip-build is also true."
    )
    parser.add_argument(
        '--debug-shell',
        action="store_true",
        help="[ADVANCED USERS ONLY] Drop into an IPython shell before building."
    )
    parser.add_argument(
        '--force',
        action="store_true",
        help="Delete and recreate VMs if they already exist"
    )
    parser.add_argument(
        '-l', '--log-file',
        help="Also log all output to this file"
    )
    parser.add_argument(
        '--packer-config',
        help="Force every VM to use this packer config instead"
    )
    parser.add_argument(
        '--list',
        nargs="*",
        help="Find all VM config files in this directory (recursive) and list any VMs found"
    )
    parser.add_argument(
        '--list-pretty',
        nargs="*",
        help="Same as --list, but pretty print the results."
    )
    parser.add_argument(
        '--breakdown',
        type=int,
        default=0,
        help="Modifier to --list or --list-pretty, but give a count of VMs by the requested granularity (e.g. 1 = CentOS; 2 = CentOS-6.2 )"
    )
    parser.add_argument(
        '--version-breakdown',
        type=int,
        default=0,
        help="Modifier to --breakdown; only consider this granularity of os_version (e.g. 1 = 6; 2 = 6.2 )"
    )
    parser.add_argument(
        '--variant',
        action="append",
        help="[Repeatable] Only build this os_variant (must be avaiable for each vm_config used in this build)"
    )
    parser.add_argument(
        '--arch',
        action="append",
        help="[Repeatable] Only build this os_arch (must be avaiable for each vm_config used in this build)."
    )
    parser.add_argument(
        '--by-name',
        action="store_true",
        help="If set, the given vm_configs are VM final name regular expressions instead of config file paths"
             " [Warning: slow and you must still use --variant if you want to"
             " restrict variants built if there are multiple variants per config file]"
    )
    parser.add_argument(
        '--files-dir',
        default=f"{os.path.dirname(os.path.realpath(__file__))}/files",
        help="Where to search for vm_config files when using --by-name"
    )
    parser.add_argument(
        '--on-error',
        default=f"cleanup",
        choices=["cleanup", "abort", "ask"],
        help="Passed to packer --on-error. Default: cleanup"
    )
    parser.add_argument(
        '--batch',
        type=int,
        default=None,
        help="Limit concurrency to this batch size. Useful if IOPS are limited or aggressive downloads trigger blacklisting. Default: Maximum concurrency"
    )
    parser.add_argument(
        '--disable-jinja',
        action="store_true",
        help="Do not render jinja inside config files (e.g. x64.yml). Never run config files from untrusted sources!"
    )
    parser.add_argument(
        '--overrides',
        help="YAML / JSON / JINJA file containing global overrides (dictionary format) for any other config that is loaded by Packinator." \
             " This does not include the packer config or --var-files, as they are passed to packer without being read." \
             " Useful for making one-off builds, but be careful not to create invalid / duplicate names."
    )

    args = parser.parse_args()
    if not (args.vm_config or args.server_only or args.list is not None or args.list_pretty is not None):
        parser.parse_args()
        raise TypeError("At least one vm_config is required if not using --server-only or --list* !")

    # --list-pretty superceds --list
    if args.list_pretty is not None:
        args.list = args.list_pretty
    if args.list is not None and len(args.list) == 0:
        # args.list is an empty list,
        #  => User wants to list using the default directory
        args.list.append(args.files_dir)

    return args

def find_vms(args, base_dirs):
    """
    Recursively search each base_dir for vm_config files.
    Returns a dictionary mapping the computed VM names to the files they were found in.

    Useful to get the config file path you need to build the VM you want
    Assumes config files are named x<ARCH>.yml
    """
    res = {}
    if not isinstance(base_dirs, list):
        base_dirs = [base_dirs]

    for base_dir in base_dirs:
        for path in Path(base_dir).rglob('x*.yml'):
            try:
                file_path = str(path)
                config = utils.load_config(
                    file_path,
                    disable_jinja=args.disable_jinja,
                    jinja_funcs={"jinja_funcs": jinja_funcs},
                    overrides=args.overrides
                )
                config_list = utils.explode_config(config)
                for config in config_list:
                    vm_name = utils.get_vm_name(
                        os_family=config["os_family"],
                        os_flavor=config["os_flavor"],
                        os_version=config["os_version"],
                        os_variant=config["os_variant"],
                        os_arch=config["os_arch"],
                    )
                    if vm_name in res:
                        print(f"WARNING: Conflicting {vm_name} in {file_path} and {res[vm_name]}")
                    else:
                        res[vm_name] = file_path
                
            except Exception as e:
                print(f"ERROR: Exception parsing {file_path}: {e}")

    return res
def find_matches(regex_list, search_list):
    """
    Returns all items in search_list that match any of the patterns in regex_list
    """

    matches = set()
    patterns = [re.compile(pattern) for pattern in regex_list]
    
    for item in search_list:
        if any(pattern.match(item) for pattern in patterns):
            matches.add(item)
    return matches

if __name__ == "__main__":

    args = get_args()
    log_level = logging.INFO

    if args.overrides:
        args.overrides = utils.load_config(
            args.overrides,
            disable_jinja=args.disable_jinja,
            jinja_funcs={"jinja_funcs": jinja_funcs},
        )

    if args.list or args.list_pretty:
        available = find_vms(args, args.list)
        if args.breakdown:
            breakdown = {}
            for key, val in available.items():
                # Increment the category count for this item
                # - separates fields
                tokens = key.split('-')
                # . separates versions, and no other field uses .
                if args.version_breakdown:
                    tokens = ['.'.join(tok.split('.')[:args.version_breakdown]) for tok in tokens]
                category = '-'.join(tokens[:args.breakdown])
                breakdown[category] = breakdown.get(category, 0) + 1 
            available = breakdown
        if args.list_pretty:
            utils.pprint_config(available, sort_keys=True)
        else:
            available = utils.sort_dict(available)
            print(json.dumps(available, indent=4))
        sys.exit(0)

    if args.by_name:
        # Translate VM names to their config files for the user
        vm_configs = []
        available = find_vms(args, args.files_dir)
        requested_keys = find_matches(args.vm_config, available.keys())
        args.vm_config = {available[key] for key in requested_keys}
        if not args.vm_config:
            print(f"FATAL: No available VMs match the given patterns!")
            sys.exit(1)
        else:
            print(f"INFO: {len(args.vm_config)} config files matched")
            if args.verbose:
                print(f"DEBUG: {args.vm_config}")

    if args.verbose:
        log_level = logging.DEBUG
    elif args.quiet:
        log_level = logging.ERROR


    pack = Packinator(
        args.config,
        serve_vars=args.include,
        packer_vars=args.var_file,
        log_level=log_level,
        log_file=args.log_file,
        force=args.force,
        packer_config=args.packer_config,
        variants=args.variant,
        architectures=args.arch,
        on_error=args.on_error,
        batch_size=args.batch,
        disable_jinja=args.disable_jinja,
        overrides=args.overrides
    )
    if args.debug_shell:
        # Only require IPython if user cares about debug shell
        import IPython
        IPython.embed()
        sys.exit(0)
    
    if args.server_only:
        pack.start_server()
    else:
        pack.build(
            *args.vm_config,
            ensure_iso  =   not args.skip_ensure_iso,
            build       =   not args.skip_build,
            post_build  =   not args.skip_post_build
        )


