# packinator

A python wrapper around [packer](https://packer.io/) that simplifies curating a large library of vSphere VM templates.

# Role

General `IAC` lifecycle:

1. \*Building VM Templates:`packinator` (`packer`)
2. Deploying VM Templates: `PyTerraDactSL` (`terraform`)
3. Configuring VM Templates: `SaltStack`
4. Managing Deployed Infrastructure
    * Guest Management: `SaltStack`
    * VM Management: `terrashell`

# Motivation

`packinator` is loosely inspired by projects like [boxcutter](https://github.com/boxcutter), which are great resources for unattended installations.
However, there are some key design differences that make `packinator` worthwhile:

* Collection-Oriented
    * `packinator` describes your entire collection of long-lived, shared VM templates
    * `boxcutter` seems focused on creating personal `Vagrant` boxes on demand
* `vSphere` Support
    * `packinator` supports `vSphere` and `vSphere` only (using `vsphere-iso`), making it suitable for self-hosted virtualization environments (where maintaining hundreds of custom templates is feasible)
    * `boxcutter` supports `Virtualbox`, `Parallels`, and `Vmware Fusion / Workstation`
* Minor Version Support
    *  `packinator` supports *all* minor versions of an OS, where feasible
    * `boxcutter` generally only supports a single VM per major version
* Custom Artifact Requirements
   * `packinator` makes it easy to apply collection-wide naming conventions, post-installation hypervisor operations, or baseline software requirements that can be tweaked to meet your needs
   * `boxcutter` has their own set of baseline requirements that would have required modification

# Requirements

* `vCenter / vSphere` (the `vSphere` API is used by `packer` and `terrashell`; free `ESXi` will not work)
* [packer](https://packer.io/) recent enough to support [vsphere-iso](https://packer.io/docs/builders/vsphere-iso.html)

These connections must be allowed by your network / firewall configuration:

![Packinator Network Diagram](docs/img/packinator_network.png "Network Communications")

# Goals / Features

## Scale

`packinator` is for building hundreds of VM templates in an organized way.
Currently, over `400` VMs (many of which are variants; e.g. `x86` / `x64`, `Server` / `Desktop`) are supported.

```json
{
    "CentOS-5"    : 42,
    "CentOS-6"    : 40,
    "CentOS-7"    : 16,
    "CentOS-8"    : 5,
    "Debian-10"   : 16,
    "Debian-7"    : 48,
    "Debian-8"    : 52,
    "Debian-9"    : 48,
    "Fedora-21"   : 4,
    "Fedora-22"   : 4,
    "Fedora-23"   : 4,
    "Fedora-24"   : 4,
    "Fedora-25"   : 4,
    "Fedora-26"   : 2,
    "Fedora-27"   : 2,
    "Fedora-28"   : 2,
    "Fedora-29"   : 2,
    "Fedora-30"   : 2,
    "Fedora-31"   : 4,
    "FreeBSD-10"  : 4,
    "FreeBSD-11"  : 8,
    "FreeBSD-12"  : 6,
    "Ubuntu-12"   : 8,
    "Ubuntu-14"   : 24,
    "Ubuntu-16"   : 24,
    "Ubuntu-18"   : 10,
    "Ubuntu-19"   : 5,
    "openSUSE-12" : 4,
    "openSUSE-15" : 6,
    "openSUSE-42" : 6,
    "pfSense-2"   : 14
}
```

## Collection-as-Code

`packinator` describes your entire template collection, serving as a lightweight backup and allowing you to rebuild it on new hardware.
Not all of your collection needs to be instantiated at once if you are low on space, but artifacts are generally expected to be long-lived.
Enforced naming / tagging conventions keep collections organized and maintainable.

## Unattended

Artifacts are created without user interaction (after the build is started). Builds should be reliable and, where possible,
not rely heavily on timing assumptions that may vary based on system / load.

## Artifact Baseline

Artifacts have vmware guest utilities (generally `open-vm-tools`) and `salt-minion` installed.

Additionally, `sshd` is enabled, and an admin user / password is set (according to your config files).

Therefore
* IP addresses get reported to `vSphere`
* If the VM has a GUI, screen resize and copy / paste work (usually)
* You can manage deployed artifacts via `Ansible` (or anything else supporting `ssh`)
* You can manage deployed artifacts via `SaltStack`

**Coming soon - PyTerraDactSL integration**

## Version Support

As long as the marginal cost for supporting addtional versions (or different architectures) is low and the artifact baseline can stil be met, `packinator` should support them.

## Hypervisor-Aware

`packinator` should leverage knowlege of the hypervisor platform (`vSphere`) when appropriate.
For example, `packinator` uploads the `.iso` files used to your datastore, enables copy / paste, applies `vSphere` tags to new VMs, etc. 

## Code Reuse / Code-as-Code

Code and configuration files are shared (instead of copied) where possible.

Infrastructure configs are separate from VM configs; for example, VM config files do not contain hypervisor file paths.

Naming conventions, file paths, etc. are all consistently computed in code, making it easy to change conventions / folder organization in the future.
Additionally, impossible-to-read and tedious-to-write `packer` `boot_command`s are generated in code, where they can be tweaked and annotated.

Decisions are made in code, allowing one set of files to describe many similar VMs.
For example, `versions < X use the archive URL scheme` or `the Y variant should have the extra 'y' package installed`.

# Setup

The [setup script](setup.sh) is tested on Ubuntu; follow the instructions / modify as necessary for your needs.

```bash
cd /path/to/packinator

./setup.sh
```

# Usage

Basic usage:
```bash
# Build the VMs described in files/Linux/CentOS/6.5/x64.json
./main.py -c packinator.json -i /path/to/vm_secrets.json --var-file /path/to/infra_secrets.json files/Linux/CentOS/6.5/x64.json 

# Build all CentOS-8 VMs  ('*' is very useful)
./main.py -c packinator.json -i /path/to/vm_secrets.json --var-file /path/to/infra_secrets.json files/Linux/CentOS/8.*/x*.json 

```
For advanced usage, see [UserGuide.md](docs/UserGuide.md)

# Development

For an overview of `packinator`'s internals, see [DeveloperGuide.md](docs/DeveloperGuide.md).
