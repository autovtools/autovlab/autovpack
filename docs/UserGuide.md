# Using Packinator

## Prereqs

Ensure that you have run the [setup script](../setup.sh) (after making any modifications necessary) and followed the instructions.

Basic knowledge of [packer](https://packer.io/intro) is assumed.

The machine running `packinator` should be isolated, but able to [communicate as required](../README.md#requirements).

The appropriate VM folder structure and datastore folder structure (for `.iso`s) should be created on your `vSphere` instance.
(`packinator` should just fail with file not found errors until you get it right)

## main.py

`main.py` is the driver for `packinator`; it's command-line features are subject to change, but you can generate an up-to-date help menu with
```bash
./main.py -h

usage: main.py [-h] [-c CONFIG] [-i INCLUDE] [--var-file VAR_FILE] [-v] [-q]
               [--server-only] [--iso-only] [--debug-shell] [--force]
               [-l LOG_FILE] [--packer-config PACKER_CONFIG]
               [--list [LIST [LIST ...]]]
               [--list-pretty [LIST_PRETTY [LIST_PRETTY ...]]]
               [--breakdown BREAKDOWN] [--version-breakdown VERSION_BREAKDOWN]
               [--variant VARIANT] [--arch ARCH] [--by-name]
               [--files-dir FILES_DIR] [--on-error {cleanup,abort,ask}]
               [--batch BATCH]
               [vm_config [vm_config ...]]

Packinator: A python wrapper around Packer to meet all your packinating needs

positional arguments:
  vm_config             A list of VM config files, one for each VM to be built

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIG, --config CONFIG
                        The packinator config file (json), shared with all VMs
  -i INCLUDE, --include INCLUDE
                        Extra packer var-files that should be shared with all
                        VMs **and the file_server**. Repeatable. (Ex. default
                        VM creds)
  --var-file VAR_FILE   Extra packer var-files that should be passed directly
                        to packer and be unavailable to Packinator.
                        Repeatable. (Ex. infrastructure creds)
  -v, --verbose         Enable debug logging
  -q, --quiet           Only log errors
  --server-only         Only start file_server
  --iso-only            Only ingest isos (as needed)
  --debug-shell         [ADVANCED USERS ONLY] Drop into an IPython shell
                        before building.
  --force               Delete and recreate VMs if they already exist
  -l LOG_FILE, --log-file LOG_FILE
                        Also log all output to this file
  --packer-config PACKER_CONFIG
                        Force every VM to use this packer config instead
  --list [LIST [LIST ...]]
                        Find all VM config files in this directory (recursive)
                        and list any VMs found
  --list-pretty [LIST_PRETTY [LIST_PRETTY ...]]
                        Same as --list, but pretty print the results.
  --breakdown BREAKDOWN
                        Modifier to --list or --list-pretty, but give a count
                        of VMs by the requested granularity (e.g. 1 = CentOS;
                        2 = CentOS-6.2 )
  --version-breakdown VERSION_BREAKDOWN
                        Modifier to --breakdown; only consider this
                        granularity of os_version (e.g. 1 = 6; 2 = 6.2 )
  --variant VARIANT     [Repeatable] Only build this os_variant (must be
                        avaiable for each vm_config used in this build)
  --arch ARCH           [Repeatable] Only build this os_arch (must be avaiable
                        for each vm_config used in this build).
  --by-name             If set, the given vm_configs are VM final name regular
                        expressions instead of config file paths [Warning:
                        slow and you must still use --variant if you want to
                        restrict variants built if there are multiple variants
                        per config file]
  --files-dir FILES_DIR
                        Where to search for vm_config files when using --by-
                        name
  --on-error {cleanup,abort,ask}
                        Passed to packer --on-error. Default: cleanup
  --batch BATCH         Limit concurrency to this batch size. Useful if IOPS
                        are limited or aggressive downloads trigger
                        blacklisting. Default: Maximum concurrency

```

## Basic Usage

See [the README](../README.md#usage)

## Listing Available VMs

To get a full list of every available VM (and the file that will build them)
```bash
./main.py --list-pretty files/
```
```json
{
    "CentOS-5.1-Dev-x64"             : "files/Linux/CentOS/5.1/x64.json",
    "CentOS-5.1-Dev-x86"             : "files/Linux/CentOS/5.1/x86.json",
    "CentOS-5.1-Server-x64"          : "files/Linux/CentOS/5.1/x64.json",
    "CentOS-5.1-Server-x86"          : "files/Linux/CentOS/5.1/x86.json",
...
}
```

To only show Ubuntu VMs:
```bash
./main.py --list-pretty files/Linux/Ubuntu
```
```json
{
    "Ubuntu-12.04.0-Dev-x64"        : "files/Linux/Ubuntu/12.04.0/x64.json",
    "Ubuntu-12.04.0-Server-x64"     : "files/Linux/Ubuntu/12.04.0/x64.json",
    "Ubuntu-12.04.1-Dev-x64"        : "files/Linux/Ubuntu/12.04.1/x64.json",
    "Ubuntu-12.04.1-Server-x64"     : "files/Linux/Ubuntu/12.04.1/x64.json",
...
}
```

To show a breakdown (count) of available VMs based on `os_flavor` (e.g. `Ubuntu` or `CentOS`):

```bash
./main.py --list-pretty files/ --breakdown 1
```
```json
{
    "CentOS"   : 103,
    "Debian"   : 164,
    "Fedora"   : 34,
    "FreeBSD"  : 18,
    "Ubuntu"   : 71,
    "openSUSE" : 16,
    "pfSense"  : 14
}
```

To show a breakdown (count) of available VMs based on OS major version:
```bash
./main.py --list-pretty files/ --breakdown 2 --version-breakdown 1
```
```json
{
    "CentOS-5"    : 42,
    "CentOS-6"    : 40,
    "CentOS-7"    : 16,
    "CentOS-8"    : 5,
...
}
```
