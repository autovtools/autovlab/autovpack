# Project Overview

 * [README.md](../README.md)
 * [docs](../docs): Contains advanced / additional docs
   * [UserGuide.md](../docs/UserGuide.md)
   * [DeveloperGuide.md](../docs/DeveloperGuide.md)
 * [examples](../examples): Contains example files
   * [packinator.json](../examples/packinator.json)
 * [setup.sh](../setup.sh): installs packinator and most dependencies
 * [setup.py](../setup.py): Allows `packinator` to be installed by `pip`
 * [main.py](../main.py): The `packinator` driver
 * [packinator](../packinator): The actual `python` module / package
     * [packinator.py](../packinator/packinator.py): The `Packinator` class definition
     * [opts](../packinator/opts): Defines user configurable options
       * [vm_opts.py](../packinator/opts/vm_opts.py)
       * [packinator_opts.py](../packinator/opts/packinator_opts.py)
     * [vm](../packinator/vm): All `VM` object definitions; one for each `os_flavor`
         * [vm.py](../packinator/vm/vm.py): The `VM` class definition
     * [utils](../packinator/utils): All helper functions used by `Packinator` and `VM`s

# Adding New VMs

## Meeting the Baseline

The OS versions that can be supported are generally the intersection of what `SaltStack` and `vmware-tools` / `open-vm-tools` support.

Useful links:
* https://repo.saltstack.com/ 
* https://get.saltstack.com/rs/304-PHQ-615/images/SaltStack-Supported-Operating-Systems.pdf
* https://github.com/vmware/open-vm-tools#how-can-i-get-involved-today

## General Workflow

1. Start at the oldest version of the distro that can possibly meet the baseline
   * Use `netinstall` `.iso`s because they are the smallest, and the user can make their own mirror if they want
2. Figure out how best to do an unattended installation for that distro
   * [boxcutter](https://github.com/boxcutter) is a good place to start for ideas
3. Use `boot_command` as little as possible; only use it to get the `kickstart` / `preseed`, if possible
   * However, type out version-specific repos as boot params, so all versions can use the same `kickstart` file
   * If you have to drop into a shell, download all the commands you need as a script
4. Get to `packer` as quickly as possible
   * Get `open-vm-tools` and `ssh` up; your `kickstart` should do the minimum needed for `packer` to connect
   * (Custom packages go in `setup.sh` as part of your baseline; let `packer` run all that for you)
4. Get a working build that meets the baseline (e.g. can check in to a `salt-master`)
   * Getting to this point is `~80%` of the effort
   * If there are OS bugs that prevent the installation, document and try again on the fixed version
5. Attempt to bump minor versions and build until you have them all
   * Throw out any versions that are broken (several just can't boot on `VMware`)
   * Carefully fix any bugs so that all the minor versions can use the same files
6. Attempt to bump major versions and repeat until you have them all
   * Try to use the same files if you can
   * Create a new set of `kickstart`, setup scripts, etc. if the old ones are incompatible

# User Configuration Options

All config options a user can define are controlled by [packinator/opts](../packinator/opts).
Some options may not be thoroughly tested; use example config files as your guide.

## PACKINATOR_OPTS
```bash
python3 -c 'import packinator.opts as opts; import json; print(json.dumps(opts.PACKINATOR_OPTS, indent=2))'
```
```json
{
  "vcenter_server": {
    "required": true,
    "description": "IP / DNS name of vCenter instance"
  },
  "vsphere_cluster": {
    "required": false,
    "description": "Name of the vSphere cluster to use"
  },
  "esxi_host": {
    "required": false,
    "description": "IP / DNS name of esxi host"
  },
  "vsphere_dc": {
    "required": true,
    "description": "Name of vSphere Datacenter"
  },
  "insecure_connection": {
    "required": false,
    "description": "Allow insecure connections (e.g. self-signed certs), default: 'True'",
    "default": "True"
  },
  "templates_root": {
    "required": true,
    "description": "The root VM folder that artifacts should be placed under. (You must make the subfolders)"
  },
  "vsphere_datastore": {
    "required": true,
    "description": "The vSphere datastore that should be used for artifacts"
  },
  "iso_root": {
    "required": false,
    "description": "The datastore ISO folder. Checked before re-downloading ISOs; downloaded ISOs are uploaded here."
  },
  "serve_ip": {
    "required": false,
    "description": "The ip to serve files on (default: 0.0.0.0)",
    "default": "0.0.0.0"
  },
  "serve_port": {
    "required": false,
    "description": "The port to serve files on. (default: 8080)",
    "default": "8080"
  },
  "serve_dir": {
    "required": false,
    "description": "The file directory to serve"
  },
  "network": {
    "required": true,
    "description": "The vSphere port group to connect artifacts to whlie they are being provisioned. (Needs outbound internet)"
  },
  "desktop_vram": {
    "required": false,
    "description": "Amount of video ram for non-headless VMs. (>=32 for 4k support)",
    "default": "32"
  },
  "server_vram": {
    "required": false,
    "description": "Amount of video ram for headless VMs",
    "default": "4"
  },
  "linux_ram": {
    "required": false,
    "description": "The amount of RAM (MB) to give *nix VMs"
  },
  "linux_cpus": {
    "required": false,
    "description": "The number of vCPUs to give *nix VMs"
  },
  "linux_disk": {
    "required": false,
    "description": "The disk size (MB) for *nix VMs"
  },
  "windows_cpus": {
    "required": false,
    "description": "The number of vCPUs to give Windows VMs"
  },
  "windows_ram": {
    "required": false,
    "description": "The amount of RAM (MB) to give Windows VMs"
  },
  "windows_disk": {
    "required": false,
    "description": "The disk size (MB) for Windows VMs"
  },
  "log_level": {
    "required": false,
    "description": "The string log level used by logging module to control. [DEBUG, INFO, WARNING, ERROR, CRITICAL]"
  },
  "force": {
    "required": false,
    "description": "Delete VMs if they already exist",
    "default": false
  },
  "ip_wait_timeout": {
    "required": false,
    "description": "Packer option; How long to wait for a VM's IP to become available (i.e. exposed to VMware tools). Default: 1h",
    "default": "1h"
  },
  "ssh_wait_timeout": {
    "required": false,
    "description": "Packer option; How long to wait for a VM to accept ssh connections after its IP became available. Default: 1h",
    "default": "1h"
  }
}
```

## VM_OPTS
```bash
python3 -c 'import packinator.opts as opts; import json; print(json.dumps(opts.VM_OPTS, indent=2))'
```
```json
{
  "py_class": {
    "required": true,
    "description": "The python class to instantiate to manage the creation of this VM."
  },
  "iso_url": {
    "required": true,
    "description": "URL to installation iso"
  },
  "iso_name": {
    "required": false,
    "description": "Actual filename of iso. Use if iso_url doesn't end in the filename"
  },
  "iso_checksum": {
    "required": false,
    "description": "Checksum of the installation iso"
  },
  "iso_path": {
    "required": false,
    "description": "Full vSphere Datastore path for the installation iso."
  },
  "os_family": {
    "required": true,
    "description": "e.g. Linux, Windows, BSD, Other"
  },
  "os_flavor": {
    "required": true,
    "description": "Linux distro / Windows version"
  },
  "os_variant": {
    "required": false,
    "description": "Variant of the os (e.g Server / Desktop)"
  },
  "os_version": {
    "required": true,
    "description": "Linux release / Windows build number"
  },
  "os_arch": {
    "required": true,
    "description": "CPU architecture of the image (e.g x86 / x86_64)"
  },
  "os_guest": {
    "required": true,
    "description": "The VMware GuestOSType for GuestOS Customization."
  },
  "helpers_dest": {
    "required": false,
    "description": "Remote path to place helper functions needed by setup scripts",
    "default": "/tmp/.packer_helpers.sh"
  },
  "log_level": {
    "required": false,
    "description": "The string log level used by logging module to control. [DEBUG, INFO, WARNING, ERROR, CRITICAL]"
  },
  "helpers_src": {
    "required": false,
    "description": "Override the path to helper functions available to setup_script."
  },
  "setup_script": {
    "required": false,
    "description": "Override the path to the setup / install script for this VM."
  },
  "packer_config": {
    "required": false,
    "description": "Override the path to the packer template to use."
  },
  "unattended_cfg": {
    "required": false,
    "description": "Override the path to the preeseed.cfg / kickstart.cfg (or equivalent) file to use."
  }
}
```
